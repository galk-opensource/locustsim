import random
import math
from functions import calculate_corners, create_rtree, check_collisions, create_visual_field, weighted_circmean, \
    circmean_r_length, correct4pbc, calculate_concealment, create_pbc_polygons
from classes import Agent
import numpy as np
from scipy.stats import circvar, circmean
from shapely.geometry import Polygon, Point, LineString


#----------------------------------------------#
# align all the time agents



def create_type3_class():
    """input"""

    agents_amount = 0
    agent_size = [0.1, 0.4]
    agent_speed = 0.1
    agent_prob2walk = 1
    agent_prob2stop = 0
    angle_visual_range = 1
    angle_r1 = 0.3  # radius of too close
    angle_r2 = 0.6  # radius of ideal
    agent_memory_length = 5
    agent_alone_memory_length = 5  # if 0 always 0.4 (only now)
    agent_angle_inertia = 0.7  # was 0.2
    agent_angle_surrounding = 0.3  # was 0.3
    agent_angle_memory = 0  # was 0.2
    prob2walk_alone = 0.8
    """code"""
    agent_center2edge = [0.5 * agent_size[0], 0.5 * agent_size[1]]
    agent_alpha = math.atan(agent_center2edge[0] / agent_center2edge[1])
    agent_dimensions = {"size": agent_size, "alpha": agent_alpha, "center2edge": agent_center2edge}
    type3 = Type3(agents_amount, agent_dimensions, agent_speed, agent_prob2walk, agent_prob2stop, angle_visual_range,
                  agent_memory_length, agent_alone_memory_length, agent_angle_inertia, agent_angle_surrounding,
                  agent_angle_memory, prob2walk_alone)
    return type3


class Type3:
    """code"""

    def __init__(self, agents_amount, agent_dimensions, agent_speed, agent_prob2walk, agent_prob2stop,
                 angle_visual_range, agent_memory_length, agent_alone_memory_length, agent_angle_inertia,
                 agent_angle_surrounding, agent_angle_memory, prob2walk_alone):
        self.amount = agents_amount
        self.dimensions = agent_dimensions
        self.speed = agent_speed
        self.prob2walk = agent_prob2walk
        self.prob2stop = agent_prob2stop
        self.visual_range = angle_visual_range
        self.memory_length = agent_memory_length
        self.alone_memory_length = agent_alone_memory_length
        self.angle_inertia = agent_angle_inertia
        self.angle_surrounding = agent_angle_surrounding
        self.angle_memory = agent_angle_memory
        self.angle_error = 1 - agent_angle_memory - agent_angle_surrounding - agent_angle_inertia
        self.prob2walk_alone = prob2walk_alone
        # need to add this in the c'tor
        self.angle_r1 = 0.3  # radius of too close
        self.angle_r2 = 0.6  # radius of ideal


def create_type3_agents(arena, agents_polygons, type3, agent_number_global, agent_number_within_type):  # only one agent
    """code"""
    intersects = True
    agent_is_walking = random.randint(0, 1)
    while intersects:
        agent_position = [random.uniform(0, arena.size[0]), random.uniform(0, arena.size[1])]
        agent_angle = random.uniform(math.pi / 180, 2 * math.pi)

        agent_corners = calculate_corners(type3.dimensions['alpha'], agent_angle,
                                          agent_position, type3.dimensions['center2edge'],
                                          arena.size)

        agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
        intersects = check_collisions(agents_polygons[:agent_number_global], agent_polygon, agent_number_global)
    # todo: need to change here too type3
    agent_type3 = Agent(agent_number_global, 3, agent_number_within_type, agent_position,
                        agent_polygon, agent_angle, agent_is_walking, [None], [None] * (type3.memory_length + 1),
                        [None] * (type3.alone_memory_length + 1))
    agent_type3.state = "program"
    return agent_type3


def create_type3_agents_fix(arena, agents_polygons, type3, agent_number_global, agent_number_within_type, pos, angle):  # only one agent
    """code"""
    intersects = True
    agent_is_walking = random.randint(0, 1)
    agent_position = pos
    agent_angle = angle

    agent_corners = calculate_corners(type3.dimensions['alpha'], agent_angle,
                                      agent_position, type3.dimensions['center2edge'],
                                      arena.size)

    agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
    agent_type3 = Agent(agent_number_global, 3, agent_number_within_type, agent_position,
                        agent_polygon, agent_angle, agent_is_walking, [None], [None] * (type3.memory_length + 1),
                        [None] * (type3.alone_memory_length + 1))
    return agent_type3

def create_type3_worldview(agent_number_global, agent_position, agents, arena, agents_polygons, type3):  # in every step
    agents_rtree = create_rtree(agents_polygons, agent_number_global)
    visual_field_not_concealed = create_visual_field(agent_position, type3.visual_range, arena) # without hidding
    visual_field = calculate_concealment(agents[agent_number_global], agents, visual_field_not_concealed,
                                         agents_polygons, type3, arena)
    intersecting_agents = []
    i = -1
    for sub_visual_field in visual_field:
        i += 1
        if agents_rtree.query(sub_visual_field):
            for other_agent in agents:
                if other_agent.number_global == agent_number_global:
                    continue
                else:
                    for other_agent_sub_polygon in other_agent.polygon:
                        if sub_visual_field.intersects(other_agent_sub_polygon):
                            intersecting_agents.append(
                                {"number_global": other_agent.number_global, "is_walking": other_agent.is_walking,
                                 "angle": other_agent.angle, "position": other_agent.position})
    return intersecting_agents, visual_field  #


def find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena, agents_polygons, type3, radius):
    agents_rtree = create_rtree(agents_polygons, agent_number_global)
    visual_field_not_concealed = create_visual_field(agent_position, radius, arena)  # without hidding
    visual_field = calculate_concealment(agents[agent_number_global], agents, visual_field_not_concealed,
                                         agents_polygons, type3, arena)
    intersecting_agents = []
    i = -1
    for sub_visual_field in visual_field:
        i += 1
        if agents_rtree.query(sub_visual_field):
            for other_agent in agents:
                if other_agent.number_global == agent_number_global:
                    continue
                else:
                    for other_agent_sub_polygon in other_agent.polygon:
                        if sub_visual_field.intersects(other_agent_sub_polygon):
                            intersecting_agents.append(
                                {"number_global": other_agent.number_global, "is_walking": other_agent.is_walking,
                                 "angle": other_agent.angle, "position": other_agent.position})
    return intersecting_agents


def find_intersecting_agents_by_zones(agent_number_global, agent_position, agents, arena, agents_polygons, type3):  # in every step
    agents_within_r1 = find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena, agents_polygons, type3, type3.angle_r1)
    agents_within_r2 = find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena, agents_polygons, type3, type3.angle_r2)
    agents_within_range = find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena,
                                                              agents_polygons, type3, type3.visual_range)

    agents_ideal = [item for item in agents_within_r2 if item not in agents_within_r1]
    agents_too_far = [item for item in agents_within_range if item not in agents_within_r2]

    return agents_within_r1, agents_ideal, agents_too_far


def mean_from_vec_array(vec_array):  # vec is [[p1_x, p1_y], [p2_x, p2_y]]
    point1_x_vals = []
    point1_y_vals = []
    point2_x_vals = []
    point2_y_vals = []

    for vec in vec_array:
        point1_x_vals.append(vec[0][0])
        point1_y_vals.append(vec[0][1])
        point2_x_vals.append(vec[1][0])
        point2_y_vals.append(vec[1][1])

    vec_amount = len(vec_array)
    mean_p1_x = sum(point1_x_vals) / vec_amount
    mean_p1_y = sum(point1_y_vals) / vec_amount
    mean_p2_x = sum(point2_x_vals) / vec_amount
    mean_p2_y = sum(point2_y_vals) / vec_amount

    mean_vec = [[mean_p1_x, mean_p1_y], [mean_p2_x, mean_p2_y]]
    return mean_vec



def move_type3_agents(agent, agents, arena, agents_polygons, type3):  # loop

    intersecting_agents, visual_field = create_type3_worldview(agent.number_global, agent.position, agents, arena,
                                                               agents_polygons,
                                                               type3)
    agent_new_angle = agent.angle  # angle is in radian
    other_agents_angles = []

    for other_agent in intersecting_agents:
        other_agents_angles.append(other_agent["angle"])  # change

    if intersecting_agents:
        # other_agents_angles.append(agent.angle)  # depend if we want to include ourself in the mean calc
        agent_new_angle = circmean(other_agents_angles)

    agent_new_position = [agent.position[0] + math.cos(agent_new_angle) * type3.speed,
                          agent.position[1] + math.sin(agent_new_angle) * type3.speed]
    agent_new_position_pbc = np.squeeze(correct4pbc([agent_new_position], arena.size)[-1])
    agent_corners = calculate_corners(type3.dimensions['alpha'], agent_new_angle,
                                      agent_new_position_pbc,
                                      type3.dimensions['center2edge'], arena.size)
    agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)

    agent.angle = agent_new_angle
    agent.position = agent_new_position_pbc
    agent.polygon = agent_polygon

    return agent, visual_field
