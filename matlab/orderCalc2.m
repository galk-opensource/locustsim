% clear all


% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
% folder1 = {'/Users/knebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
% folder2 = {'/Users/knebel/Desktop/exp4/fixedWalk01fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk03fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk04fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk05fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk06fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk07fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk08fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk09fixedStop02/exp';};
folder1 = {'/Users/knebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp';};
folder2 = {'/Users/knebel/Desktop/exp4/Walk0804fixedStop02_memory1/exp';};


for ind = 1:size([folder1, folder2], 1)
    d=0;
    for i = 1:400
        data=[];
        isWalking = [];
        angle = [];
        data(1,:,:) = load([folder1{ind}, int2str(i), '.csv']);
        data(2,:,:) = load([folder2{ind}, int2str(i), '.csv']);
        for dat = 1:2
            isWalking(dat,:,:) = data(dat,5:5:end, :);
            angle(dat,:,:) = data(dat,4:5:end, :);
        end
        
        for dat = 1:2
            for in = 200:size(isWalking, 2)
                
                %         isWalkingTemp = logical(isWalking(in, :));
                %         angleTemp = angle(in, isWalkingTemp);
                angleTemp = squeeze(angle(dat,in, :));
                stepR(in) = circ_r(angleTemp);
            end
        d = d+1;
                    %         stepR_smooth(i, :) = movmean(stepR(i, :), 10);
        clear isWalkingTemp angleTemp
        %         order = nanmean(stepR_smooth');
        order(d) = nanmean(stepR');
        %         order = var(stepR');
        %         order = var(stepR_smooth');
        end  
    end
    numOfRep = 40;
    d=0;
    for i = 81:numOfRep:800
        d=d+1;
        results{ind}.Divided(d, 1:numOfRep) = order(i:i+numOfRep-1);
        results{ind}.median_Divided = median(results{ind}.Divided');
        results{ind}.prc25 = results{ind}.median_Divided - prctile(results{ind}.Divided', 25);
        results{ind}.prc75 = prctile(results{ind}.Divided', 75) - results{ind}.median_Divided;
    end
end

