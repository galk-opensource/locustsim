% clear all


% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
folder1 = {'/Users/knebel/Desktop/exp3/fixedWalk02fixedStop01/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop03/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop04/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop05/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop06/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop07/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop08/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop09/exp';};
folder2 = {'/Users/knebel/Desktop/exp4/fixedWalk02fixedStop01/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop02/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop03/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop04/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop05/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop06/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop07/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop08/exp';
    '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop09/exp';};
% folder = {'/Users/danielknebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp';};


for ind = 1:length([folder1, folder2])
    for i = 1:800
        data(1,:,:) = load([folder1{ind}, int2str(i), '.csv']);
        data(2,:,:) = load([folder2{ind}, int2str(i), '.csv']);
        for dat = 1:2
            isWalking = data(5:5:end, :);
            angle = data(4:5:end, :);

        for in = 1:size(isWalking, 1)
            %         isWalkingTemp = logical(isWalking(in, :));
            %         angleTemp = angle(in, isWalkingTemp);
            angleTemp = angle(in, :);
            stepR(i, in) = circ_r(angleTemp');
        end
        stepR_smooth(i, :) = movmean(stepR(i, :), 10);
        clear isWalkingTemp angleTemp
        %         order = nanmean(stepR_smooth');
                order = nanmean(stepR');
%         order = var(stepR');
%         order = var(stepR_smooth');
        
    end
    numOfRep = 20;
    d=0;
    for i = 41:numOfRep:800
        d=d+1;
        results{ind}.Divided((d), 1:numOfRep) = order(i:i+numOfRep-1);
        results{ind}.median_Divided = median(results{ind}.Divided');
        results{ind}.prc25 = results{ind}.median_Divided - prctile(results{ind}.Divided', 25);
        results{ind}.prc75 = prctile(results{ind}.Divided', 75) - results{ind}.median_Divided;
    end
end

