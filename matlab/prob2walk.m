for i = 1:400
    data = load(['/Users/danielknebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp', int2str(i), '.csv']);
    isWalking = data(5:5:end, :);
    for in = 1:size(isWalking, 2)
        tempIsWalking = isWalking(:, in);
        walkPos = tempIsWalking == 1;
        afterWalk = logical([0; walkPos(1:end-1)]);
        probWalk{i, in} = sum(tempIsWalking(afterWalk) == 1) / sum(walkPos);
    end
end

probWalk(cellfun(@isempty, probWalk)) = {nan};
a = cell2mat(probWalk);
aa = nanmean(a');
b=0;
for i = 1:20:400
b=b+1;
c(b) = nanmean(aa(i:i+19));
end