clearvars -except measurements data_single
fps = 25/3;
firstFrame = round(1);
lastFrame = round(10*60*fps);

for i = 1:1:20
frames(i) = length(data_single{i}{1}.x);
end

data=data_single(frames > 65000);

for i = 1:length(data)
    temp = data{i}{1}.isWalking;
    first(i) = sum(temp(firstFrame:lastFrame))/(lastFrame-firstFrame);
    mid(i) = sum(temp(60*60*fps+firstFrame:60*60*fps+lastFrame))/(lastFrame-firstFrame);
    last(i) = sum(temp(120*60*fps+firstFrame:120*60*fps+lastFrame))/(lastFrame-firstFrame);
end
