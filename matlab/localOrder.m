clear all
VR = 1;

% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};

folder = {'/Users/knebel/Desktop/exp3/fixedWalk02fixedStop01/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop03/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop04/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop05/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop06/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop07/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop08/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop09/exp';};
% folder = {'/Users/danielknebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp';};


for ind = 1:length(folder)
    for i = 1:400
        data = load([folder{ind}, int2str(i), '.csv']);
        localOrderWithMe = NaN(1000, length(2*i));
        localOrderWithoutMe = NaN(1000, length(2*i));
        numOfFriends = NaN(1000, length(2*i));
        xs = data(2:5:end,:);
        ys = data(3:5:end,:);
        angle = data(4:5:end,:);
        localOrderWithMe = NaN(1000, length(xs(1,:)));
        localOrderWithoutMe = NaN(1000, length(xs(1,:)));
        numOfFriends = NaN(1000, length(xs(1,:)));
        for in = 1:1000
            xMat = repmat(xs(in,:), length(xs(in,:)), 1);
            yMat = repmat(ys(in,:), length(ys(in,:)), 1);
            xMatI = xMat';
            yMatI = yMat';
            xDist = abs(xMat - xMatI);
            yDist = abs(yMat - yMatI);
            xDist(xDist>5) = 10 - xDist(xDist>5);
            yDist(yDist>5) = 10 - yDist(yDist>5);
            dists = sqrt((xDist.^2 + yDist.^2));
            for inde = 1:length(dists)
                friends = find(dists(inde,:) < VR);
                friends(friends == inde) = [];
                numOfFriends(in,inde) = length(friends);
                if numOfFriends(in,inde) > 1
                    localOrderWithoutMe(in,inde) = circ_r(angle(in, friends)');
                    localOrderWithMe(in,inde) = circ_r(angle(in, [inde, friends])');
                elseif numOfFriends(in,inde) > 0
                    localOrderWithMe(in,inde) = circ_r(angle(in, [inde, friends])');
                end
            end
        end
        meanNumOfFriends(i) = nanmean(nanmean(numOfFriends));
        meanLocalOrderWithMe(i) = nanmean(nanmean(localOrderWithMe));
        meanLocalOrderWithoutMe(i) = nanmean(nanmean(localOrderWithoutMe));
        
        

    end
    d=0;
    numOfRep = 20;
    for i = 41:numOfRep:400
        d=d+1;
        results1{ind}.Divided(d, 1:numOfRep) = meanNumOfFriends(i:i+numOfRep-1)';
        results1{ind}.median_Divided = nanmedian(results1{ind}.Divided');
        results1{ind}.prc25 = results1{ind}.median_Divided - prctile(results1{ind}.Divided', 25);
        results1{ind}.prc75 = prctile(results1{ind}.Divided', 75) - results1{ind}.median_Divided;
        
        results2{ind}.Divided(d, 1:numOfRep) = meanLocalOrderWithMe(i:i+numOfRep-1)';
        results2{ind}.median_Divided = nanmedian(results2{ind}.Divided');
        results2{ind}.prc25 = results2{ind}.median_Divided - prctile(results2{ind}.Divided', 25);
        results2{ind}.prc75 = prctile(results2{ind}.Divided', 75) - results2{ind}.median_Divided;
        
        
        results3{ind}.Divided(d, 1:numOfRep) = meanLocalOrderWithoutMe(i:i+numOfRep-1)';
        results3{ind}.median_Divided = nanmedian(results3{ind}.Divided');
        results3{ind}.prc25 = results3{ind}.median_Divided - prctile(results3{ind}.Divided', 25);
        results3{ind}.prc75 = prctile(results3{ind}.Divided', 75) - results3{ind}.median_Divided;
    end
end
