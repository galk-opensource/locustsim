for i = 1:400
    data = load(['/Users/danielknebel/Desktop/exp2/fixedWalk05fixedStop05/exp', int2str(i), '.csv']);
    isWalking = data(5:5:end, :);
    for in = 1:size(isWalking, 2)
        tempIsWalking = isWalking(:, in);
        stopPos = tempIsWalking == 0;
        afterStop = logical([0; stopPos(1:end-1)]);
        probStop{i, in} = sum(tempIsWalking(afterStop) == 0) / sum(stopPos);
    end
end

probStop(cellfun(@isempty, probStop)) = {nan};
a = cell2mat(probStop);
aa = nanmean(a');
b=0;
for i = 1:20:400
b=b+1;
c(b) = nanmean(aa(i:i+19));
end