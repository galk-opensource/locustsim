clear all
VR = 1;

% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};

% folder1 = {'/Users/knebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
% folder2 = {'/Users/knebel/Desktop/exp4/fixedWalk01fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk03fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk04fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk05fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk06fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk07fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk08fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk09fixedStop02/exp';};
folder1 = {'/Users/knebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp';};
folder2 = {'/Users/knebel/Desktop/exp4/Walk0804fixedStop02_memory1/exp';};


for ind = 1:size([folder1, folder2], 1)
    d = 0;
    for i = 1:400
        data=[];
        data(1,:,:) = load([folder1{ind}, int2str(i), '.csv']);
        data(2,:,:) = load([folder2{ind}, int2str(i), '.csv']);
        localOrderWithMe = NaN(1000, length(2*i));
        localOrderWithoutMe = NaN(1000, length(2*i));
        numOfFriends = NaN(1000, length(2*i));
        for dat = 1:2
            xs = squeeze(data(dat, 2:5:end,:));
            ys = squeeze(data(dat, 3:5:end,:));
            angle = squeeze(data(dat, 4:5:end,:));
            d = d+1;
            for in = 200:1000
                xMat = repmat(xs(in,:), length(xs(in,:)), 1);
                yMat = repmat(ys(in,:), length(ys(in,:)), 1);
                xMatI = xMat';
                yMatI = yMat';
                xDist = abs(xMat - xMatI);
                yDist = abs(yMat - yMatI);
                xDist(xDist>5) = 10 - xDist(xDist>5);
                yDist(yDist>5) = 10 - yDist(yDist>5);
                dists = sqrt((xDist.^2 + yDist.^2));
                for inde = 1:length(dists)
                    friends = find(dists(inde,:) < VR);
                    friends(friends == inde) = [];
                    numOfFriends(in,inde) = length(friends);
                    if numOfFriends(in,inde) > 1
                        localOrderWithoutMe(in,inde) = circ_r(angle(in, friends)');
                        localOrderWithMe(in,inde) = circ_r(angle(in, [inde, friends])');
                    elseif numOfFriends(in,inde) > 0
                        localOrderWithMe(in,inde) = circ_r(angle(in, [inde, friends])');
                    end
                end
            end
        end
        meanNumOfFriends(d) = nanmean(nanmean(numOfFriends));
        meanLocalOrderWithMe(d) = nanmean(nanmean(localOrderWithMe));
        meanLocalOrderWithoutMe(d) = nanmean(nanmean(localOrderWithoutMe));
        
        

    end
    d=0;
    numOfRep = 40;
    for i = 81:numOfRep:800
        d=d+1;
        results1{ind}.Divided(d, 1:numOfRep) = meanNumOfFriends(i:i+numOfRep-1)';
        results1{ind}.median_Divided = nanmedian(results1{ind}.Divided');
        results1{ind}.prc25 = results1{ind}.median_Divided - prctile(results1{ind}.Divided', 25);
        results1{ind}.prc75 = prctile(results1{ind}.Divided', 75) - results1{ind}.median_Divided;
        
        results2{ind}.Divided(d, 1:numOfRep) = meanLocalOrderWithMe(i:i+numOfRep-1)';
        results2{ind}.median_Divided = nanmedian(results2{ind}.Divided');
        results2{ind}.prc25 = results2{ind}.median_Divided - prctile(results2{ind}.Divided', 25);
        results2{ind}.prc75 = prctile(results2{ind}.Divided', 75) - results2{ind}.median_Divided;
        
        
        results3{ind}.Divided(d, 1:numOfRep) = meanLocalOrderWithoutMe(i:i+numOfRep-1)';
        results3{ind}.median_Divided = nanmedian(results3{ind}.Divided');
        results3{ind}.prc25 = results3{ind}.median_Divided - prctile(results3{ind}.Divided', 25);
        results3{ind}.prc75 = prctile(results3{ind}.Divided', 75) - results3{ind}.median_Divided;
    end
end
