clearvars -except measurements data_single

fps = 25/3;
firstFrame = round(1);
lastFrame = round(10*60*fps);
for i = 1:1:20
frames(i) = length(data_single{i}{1}.x);
end
mea=measurements(frames > 65000);

for i = 1:length(mea)
    temp = mea{i}.speedXY;
    temp2 = mea{i}.isWalking;
    temp(temp2 == 0) = nan;
    first(i) = nanmean(temp(10*60+10*60+firstFrame:10*60+10*60+lastFrame));
    mid(i) = nanmean(temp(60*60*fps+firstFrame:60*60*fps+lastFrame));
    last(i) = nanmean(temp(120*60*fps+firstFrame:120*60*fps+lastFrame));
end