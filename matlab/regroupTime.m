VR = 1
memory = 0
for i = 21:400
    data = load(['/Users/danielknebel/Desktop/exp2/fixedWalk05fixedStop02/exp', int2str(i), '.csv']);
    timeToRegroup{i} = [];
    aloneAnimals = {};
    alone = [];
    xs = data(2:5:end,:);
    ys = data(3:5:end,:);
    for in = 1:1000
        xMat = repmat(xs(in,:), length(xs(in,:)), 1);
        yMat = repmat(ys(in,:), length(ys(in,:)), 1);
        xMatI = xMat';
        yMatI = yMat';
        xDist = abs(xMat - xMatI);
        yDist = abs(yMat - yMatI);
        xDist(xDist>5) = 10 - xDist(xDist>5);
        yDist(yDist>5) = 10 - yDist(yDist>5);
        
        dists = sqrt((xDist.^2 + yDist.^2));
        outVR = zeros(size(dists));
        outVR(dists > VR) = 1;
        alone(in, :) = sum(outVR) == 1;
        
        if in > memory
            aloneAnimals{in} = find(alone(in-memory:in, :) == 1);
        end
        if in > memory + 1
            sameAnimals = ismember(aloneAnimals{in-1}, aloneAnimals{in});
            regroupedAnimals = aloneAnimals{in -1}(sameAnimals == 0);
            for ind = 1:length(regroupedAnimals)
                animal = regroupedAnimals(ind);
                flag = 0;
                inde = memory;
                while flag == 0
                    if inde >= in + memory - 1
                        flag = 2;
                    elseif ismember(animal, aloneAnimals{in - inde - 1})
                        inde = inde + 1;
                    else
                        flag = 1;
                    end
                end
                if flag == 2
                    timeToRegroup{i} = [timeToRegroup{i}, inde - memory];
                end   
            end
        end
    end
    meanTimeToRegroup(i) = mean(timeToRegroup{i});
end



in = 1;
numOfRep = 20;
for i = 1:numOfRep:400
    results{in}.Divided((i+numOfRep-1)/numOfRep, 1:numOfRep) = (meanTimeToRegroup(i:i+numOfRep-1)');
    results{in}.median_Divided = nanmedian(results{in}.Divided');
    results{in}.prc25 = results{in}.median_Divided - prctile(results{in}.Divided', 25);
    results{in}.prc75 = prctile(results{in}.Divided', 75) - results{in}.median_Divided;
end

