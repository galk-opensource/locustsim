clearvars -except measurements data_single

fps = 25/3;
firstFrame = round(1);
lastFrame = round(10*60*fps);
for i = 1:1:20
frames(i) = length(data_single{i}{1}.x);
end
mea=measurements(frames > 65000);

for i = 1:length(mea)
    temp = mea{i}.walkingBout.duration{1};
    temp2 = mea{i}.walkingBout.starts{1};
    first(i) = nanmean(temp(10*60+firstFrame<temp2 & temp2<10*60+lastFrame));
    mid(i) = nanmean(temp(60*60*fps+firstFrame<temp2 & temp2<60*60*fps+lastFrame));
    last(i) = nanmean(temp(120*60*fps+firstFrame<temp2 & temp2<120*60*fps+lastFrame));
end