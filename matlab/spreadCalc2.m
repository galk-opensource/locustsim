% clear all


% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
% folder1 = {'/Users/knebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/knebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
% folder2 = {'/Users/knebel/Desktop/exp4/fixedWalk01fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk02fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk03fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk04fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk05fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk06fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk07fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk08fixedStop02/exp';
%     '/Users/knebel/Desktop/exp4/fixedWalk09fixedStop02/exp';};
folder1 = {'/Users/knebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp';};
folder2 = {'/Users/knebel/Desktop/exp4/Walk0804fixedStop02_memory1/exp';};


for ind = 1:size([folder1, folder2], 1)
    d=0;
    for i = 1:400
        
        data=[];

        data(1,:,:) = load([folder1{ind}, int2str(i), '.csv']);
        data(2,:,:) = load([folder2{ind}, int2str(i), '.csv']);
        for dat = 1:2
            xs = squeeze(data(dat,2:5:end,:));
            ys = squeeze(data(dat,3:5:end,:));
            d=d+1;
            for in = 200:1000
                xMat = repmat(xs(in,:), length(xs(in,:)), 1);
                yMat = repmat(ys(in,:), length(ys(in,:)), 1);
                xMatI = xMat';
                yMatI = yMat';
                xDist = tril(abs(xMat - xMatI), -1);
                yDist = tril(abs(yMat - yMatI), -1);
                xDist = reshape(xDist, 1, []);
                yDist = reshape(yDist, 1, []);
                xDist(xDist == 0) = [];
                yDist(yDist == 0) = [];
                xDist(xDist>5) = 10 - xDist(xDist>5);
                yDist(yDist>5) = 10 - yDist(yDist>5);
                dists = sqrt(xDist.^2 + yDist.^2);
                spread(d,in) = mean(dists);
                %             spread(i,in) = var(dists);
            end
        end
    end
    numOfRep = 40;
    d=0;
    for i = 81:numOfRep:800
        d=d+1;
        results{ind}.Divided(d, 1:numOfRep) = mean(spread(i:i+numOfRep-1, :)');
        results{ind}.median_Divided = median(results{ind}.Divided');
        results{ind}.prc25 = results{ind}.median_Divided - prctile(results{ind}.Divided', 25);
        results{ind}.prc75 = prctile(results{ind}.Divided', 75) - results{ind}.median_Divided;
    end
end


