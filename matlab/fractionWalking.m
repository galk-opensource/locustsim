clear all
% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};

folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop01/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop03/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop04/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop05/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop06/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop07/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop08/exp';
    '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop09/exp';};

for ind=1:length(folder)
    for i = 1:400
        data = load([folder{ind}, int2str(i), '.csv']);
        isWalking = data(5:5:end, :);
        frac(i) = sum(reshape(isWalking,1,[]))/numel(isWalking);
        diffIsWalking = diff(isWalking);
        for inde = 1:1:size(diffIsWalking, 2)
            values = [];
            values = find(diffIsWalking(:,inde) ~= 0);
            dd = 0;
            while diffIsWalking(values(1), inde) == 0
                values(1) = [];
            end
            if diffIsWalking(values(1), inde) == 1
                for in = 1:2:length(values) - 1
                    dd= dd + 1;
                    walkingBouts{i,inde}(dd) = values(in+1) - values(in);
                    if in > 1
                        pauses{i,inde}(dd) = values(in) - values(in-1);
                    end
                end
            elseif diffIsWalking(values(1), inde) == -1
                for in = 1:2:length(values) - 1
                    dd = dd + 1;
                    pauses{i,inde}(dd) = values(in+1) - values(in);
                    if in > 1
                        walkingBouts{i,inde}(dd) = values(in) - values(in-1);
                    end
                end
            end
            walkingBoutsMean{i}(inde) = mean(walkingBouts{i,inde});
            pausesMean{i}(inde) = mean(pauses{i});
        end
        walkingBoutMeanMean(i) = mean(walkingBoutsMean{i});
        pausesMeanMean(i) = mean(pausesMean{i});
    end
    d=0;
    for i = 1:20:400
        d=d+1;
        fracWalking{ind}(d) = median(frac(i:i+19));
        walkingBoutTotal{ind}(d) = median(walkingBoutMeanMean(i:i+19));
        pausesMeanTotal{ind}(d) = median(pausesMeanMean(i:i+19));
    end
%     totalFracWalking(ind) = mean(frac);
end

