
% folder = {'/Users/danielknebel/Desktop/exp3/fixedWalk01fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk03fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk04fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk05fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk06fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk07fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk08fixedStop02/exp';
%     '/Users/danielknebel/Desktop/exp3/fixedWalk09fixedStop02/exp';};
folder = {'/Users/knebel/Desktop/exp3/fixedWalk02fixedStop01/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop02/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop03/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop04/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop05/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop06/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop07/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop08/exp';
    '/Users/knebel/Desktop/exp3/fixedWalk02fixedStop09/exp';};
% folder = {'/Users/danielknebel/Desktop/exp3/Walk0804fixedStop02_memory1/exp';}

for ind = 4:length(folder)
    for i = 1:400
        data = load([folder{ind}, int2str(i), '.csv']);
        xs = data(2:5:end,:);
        ys = data(3:5:end,:);
        for in = 1:1000
            xMat = repmat(xs(in,:), length(xs(in,:)), 1);
            yMat = repmat(ys(in,:), length(ys(in,:)), 1);
            xMatI = xMat';
            yMatI = yMat';
            xDist = tril(abs(xMat - xMatI), -1);
            yDist = tril(abs(yMat - yMatI), -1);
            xDist = reshape(xDist, 1, []);
            yDist = reshape(yDist, 1, []);
            xDist(xDist == 0) = [];
            yDist(yDist == 0) = [];
            xDist(xDist>5) = 10 - xDist(xDist>5);
            yDist(yDist>5) = 10 - yDist(yDist>5);
            dists = sqrt(xDist.^2 + yDist.^2);
            spread(i,in) = mean(dists);   
%             spread(i,in) = var(dists);
        end
    end
    numOfRep = 20;
    d=0;
    for i = 41:numOfRep:400
        d=d+1;
        results{ind}.Divided(d, 1:numOfRep) = median(spread(i:i+numOfRep-1, :)');
        results{ind}.median_Divided = median(results{ind}.Divided');
        results{ind}.prc25 = results{ind}.median_Divided - prctile(results{ind}.Divided', 25);
        results{ind}.prc75 = prctile(results{ind}.Divided', 75) - results{ind}.median_Divided;
    end
end


