
animalNum = 1;
jumpsBetweenFrames = 8;

x = animals{animalNum}.x(1:jumpsBetweenFrames:end);
y = animals{animalNum}.y(1:jumpsBetweenFrames:end);

diffTheta = diff(unwrap(atan2(x, y)));