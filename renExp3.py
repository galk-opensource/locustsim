import initiation
import simulate
from datetime import datetime
import os

folder = '/Users/knebel/Desktop/exp2/FixedVarStop_notFixedWalk_aloneMemory0'
os.mkdir(folder)
initial_animals_amout = 2
max_animals_amount = 42
jumps_in_animal_amount = 2
number_of_reps = 20
g = 0
stop_probs = [0.452459186609300, 0.400030860055981, 0.334639098494970, 0.312215738229445, 0.279770978590580,
              0.254869207117148, 0.241279135479577, 0.211182861757405, 0.208506815869102, 0.203414260109932,
              0.197725837098808, 0.194115935229064, 0.188433315094122, 0.188703712317894, 0.183431208246325,
              0.178927983623373, 0.188651140785379, 0.189115898373176, 0.189523060923062, 0.188183714275213] #exp2/notFixedWalk_aloneMemory0

# [0.457082440052255, 0.351476721723621, 0.303134064637744, 0.259169042737509, 0.220456795495757,
#       0.191296145149385, 0.169275479790094, 0.157515172583733, 0.147752152117511, 0.140287646847928,
#       0.129319745735147, 0.120657806347177, 0.132812085518622, 0.114977003313323, 0.123002618558006,
#       0.113397215117357, 0.124298918142137, 0.115770525543929, 0.115007905380591, 0.113639617440771] # exp/notFixStop_notFixedWalk

for number_of_animals, stop_prob in zip(list(range(initial_animals_amout, max_animals_amount, jumps_in_animal_amount)),
                                        stop_probs):
    for i in range(number_of_reps):
        g += 1
        arena = initiation.create_arena()
        types = initiation.create_types()
        types[0].type_class.amount = number_of_animals
        types[0].type_class.prob2stop = stop_prob
        simulation = initiation.create_simulation(types)
        agents, simulation = initiation.create_agents(arena, types, simulation)
        name = datetime.now()
        name = 'exp' + str(g)
        file = open(folder + '/' + name + '.txt', 'w')
        for attr in dir(types[0].type_class):
            if hasattr(types[0].type_class, attr):
                file.write(folder + "/type_class.%s = %s \n" % (attr, getattr(types[0].type_class, attr)))
        file.close()
        txtFile = ''
        csvFile = name + '.csv'
        agents, simulation = simulate.create_simulation(simulation, arena, types, agents, 0, folder + '/' + csvFile)
