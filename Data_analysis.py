# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean


# folders structure:
#
#   Eden_Experiments
#       type1
#           set1 (of angles and positions)
#               model_x
#                   run1


def save_data_to_file(path, changes_per_step, angles_per_step):
    csvfile = open((path + '/exp_data.csv'), 'w', newline='')
    obj = csv.writer(csvfile)
    obj.writerows([changes_per_step, angles_per_step])
    csvfile.close()


def read_data_from_file(path):
    csvfile = open((path + '/exp_data.csv'), 'r', newline='')
    obj = csv.reader(csvfile)
    changes_per_step = next(obj)
    changes_per_step = list(map(int, changes_per_step))
    angles_per_step = next(obj)
    for i in range(len(angles_per_step)):
        angles_per_step[i] = ast.literal_eval(angles_per_step[i])
        angles_per_step[i] = list(map(float, angles_per_step[i]))
    pos_per_step = next(obj)
    for step in range(len(pos_per_step)):
        pos_per_step[step] = pos_per_step[step].replace("[array(", "")
        pos_per_step[step] = pos_per_step[step].replace("array(", "")
        pos_per_step[step] = pos_per_step[step].replace(")]", "")
        pos_per_step[step] = pos_per_step[step].replace(")", "")
        pos_per_step[step] = pos_per_step[step].replace(" ", "")
        pos_per_step[step] = pos_per_step[step].replace("],", "]|")
        pos_per_step[step] = pos_per_step[step].split("|")
        for i in range(len(pos_per_step[step])):
            pos_per_step[step][i] = ast.literal_eval(pos_per_step[step][i])
        # pos_per_step[i] = list(map(float, pos_per_step[i]))
    csvfile.close()
    return changes_per_step, angles_per_step, pos_per_step


def create_combined_graphs(path, data_name, models_to_compare, all_data):
    steps = list(range(len(all_data[models_to_compare[0]])))

    combined_dic = {}
    combined_dic['step'] = steps
    for model_name in models_to_compare:
        combined_dic[model_name] = all_data[model_name]
    df = DataFrame(combined_dic)

    ax = plt.gca()

    # colors = ['']

    for model_name in models_to_compare:
        df.plot(kind='line', x='step', y=model_name, ax=ax, figsize=(8,8))
        # df.plot(kind='bar', x='step', y=model_name, ax=ax, figsize=(8,8))

    plt.ylabel(data_name)
    # plt.show()
    plt.savefig((path + "/combined-" + data_name))
    plt.gca().clear()
    print()


def angles_per_step_to_variance_per_step(angles_per_step):
    variance_per_step = []

    for angles in angles_per_step:
        mean_ang = circmean(angles)
        variance = circvar(angles)
        variance_per_step.append(variance)

    return variance_per_step


def angles_per_step_to_order_parameter(angles_per_step):
    order_parameter = 0.0
    variance_per_step = []
    avg_ang_per_step = []

    for angles in angles_per_step:
        mean_ang = circmean(angles)
        variance = circvar(angles)
        variance_per_step.append(variance)

    return variance_per_step


def create_regular_graphs(path, changes_per_step, angles_per_step):
    steps = list(range(len(changes_per_step)))

    angle_changes_Data = {'Step': steps,
            'Angle_changes': changes_per_step
            }

    angle_changes_df = DataFrame(angle_changes_Data, columns=['Step', 'Angle_changes'])
    angle_changes_df.plot(x='Step', y='Angle_changes', kind='line')

    # plt.show()
    plt.savefig((path + "/angle_changes"))
    print()

    #---------------------------------------------#

    variance_per_step = angles_per_step_to_variance_per_step(angles_per_step)

    angle_variance_Data = {'Step': steps,
                          'Angle_variance': variance_per_step
                          }

    angle_variance_df = DataFrame(angle_variance_Data, columns=['Step', 'Angle_variance'])
    angle_variance_df.plot(x='Step', y='Angle_variance', kind='line')

    # plt.show()
    plt.savefig((path + "/angle_variance"))
    print()

    # ---------------------------------------------#

    angles_last_step = angles_per_step[-1]
    angles_last_step = ['%.2f' % elem for elem in angles_last_step]
    diff_angles = list(dict.fromkeys(angles_last_step))
    diff_angles.sort()
    # angles_degree = []
    angles_count = []
    for angle in diff_angles:
        # angles_degree.append(angle * 180 / math.pi)
        angles_count.append(angles_last_step.count(angle))

    # degrees_diff_angles = list(dict.fromkeys(angles_degree))
    # if not len(degrees_diff_angles) == len(diff_angles):
    #     print("BUGGGGGG")

    angles_last_step_Data = {'Angles': diff_angles,
                          'Angles_last_step': angles_count
                          }

    angles_last_step_df = DataFrame(angles_last_step_Data, columns=['Angles', 'Angles_last_step'])
    angles_last_step_df.plot(x='Angles', y='Angles_last_step', kind='bar', figsize=(8,8))

    # plt.show()
    plt.savefig((path + "/angles_last_step"))
    print()


def create_variance_combined_graphs(set_num):
    path = "Eden_Experiments"

    # exp_types = [10, 20, 30]

    exp_types = [10]

    curr_path = path

    for exp_type in exp_types:

        num_of_agents = int(exp_type)
        exp_type_name = str(exp_type) + "_agents_same_type"
        curr_path = path + "/" + exp_type_name

        num_of_sets = 1
        for i in range(num_of_sets):
            i = set_num
            data_name = 'angle_variance'
            # models_to_compare = ["daniel's model", "handles crowded & lonely", "always align","handles only crowded","handles only lonely"]
            models_to_compare = ["handles crowded & lonely", "always align", "handles only crowded",
                                 "handles only lonely"]

            all_data = {}

            curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1))
            set_path = curr_path

            # type 0 - Daniel's model
            # type 2 - handles both crowded and lonely
            # type 3 - align all the time
            # type 4 - handles only crowded
            # type 5 - handles only lonely

            for model_name in models_to_compare:

                curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1)) + "/" + model_name

                wanted_run = 0
                curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1)) + "/" + model_name + "/" + ("run" + str(wanted_run))
                changes_per_step, angles_per_step, pos_per_step = read_data_from_file(curr_path)
                variance_per_step = angles_per_step_to_variance_per_step(angles_per_step)
                all_data[model_name] = variance_per_step

            create_combined_graphs(set_path, data_name, models_to_compare, all_data)


def angles_distance(angle1, angle2):
    unit1 = math.degrees(angle1)
    unit2 = math.degrees(angle2)
    phi = abs(unit2-unit1) % 360
    sign = 1
    # used to calculate sign
    if not ((unit1-unit2 >= 0 and unit1-unit2 <= 180) or (
            unit1-unit2 <= -180 and unit1-unit2 >= -360)):
        sign = -1
    if phi > 180:
        result = 360-phi
    else:
        result = phi

    return math.radians(result)


def angles_per_step_to_size_of_change_per_step(angles_per_step):
    size_of_change_per_step = []
    for i in range(len(angles_per_step)):
        if i > 0:
            size_of_change = 0.0
            old_angles = angles_per_step[i-1]
            new_angles = angles_per_step[i]
            for j in range(len(old_angles)):
                size_of_change = size_of_change + angles_distance(old_angles[j], new_angles[j])
            size_of_change_per_step.append(size_of_change)
    return size_of_change_per_step


def create_size_of_change_combined_graphs(set_num):
    path = "Experiments"

    # exp_types = [10, 20, 30]

    exp_types = [20]

    curr_path = path

    for exp_type in exp_types:

        num_of_agents = int(exp_type)
        exp_type_name = str(exp_type) + "_agents_same_type"
        curr_path = path + "/" + exp_type_name

        num_of_sets = 1
        for i in range(num_of_sets):
            i = set_num
            data_name = 'size_of_change'
            #models_to_compare = ["daniel's model", "handles crowded & lonely", "always align","handles only crowded","handles only lonely"]
            models_to_compare = ["handles crowded & lonely", "always align", "handles only crowded",
                                 "handles only lonely"]
            # models_to_compare = ["daniel's model"]
            all_data = {}

            curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1))
            set_path = curr_path

            # type 0 - Daniel's model
            # type 2 - handles both crowded and lonely
            # type 3 - align all the time
            # type 4 - handles only crowded
            # type 5 - handles only lonely

            for model_name in models_to_compare:
                curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1)) + "/" + model_name

                wanted_run = 0
                curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1)) + "/" + model_name + "/" + (
                            "run" + str(wanted_run))
                changes_per_step, angles_per_step, pos_per_step = read_data_from_file(curr_path)
                size_of_change_per_step = angles_per_step_to_size_of_change_per_step(angles_per_step)
                all_data[model_name] = size_of_change_per_step

            create_combined_graphs(set_path, data_name, models_to_compare, all_data)



create_variance_combined_graphs(1)
#create_size_of_change_combined_graphs(7)
#simulate.restore_simulation("Experiments/10_agents_same_type/set2/daniel's model/run0")
