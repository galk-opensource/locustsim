# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import sys
import sim_data


num_of_agents = int(sys.argv[1])
set_path = sys.argv[2]
model_name = sys.argv[3]

# num_of_agents = 10
# set_path = "Eden_Experiments/10_agents_same_type/set1"
# model_name = "handles crowded & lonely"

num_of_runs = 1
if model_name == "daniel's model":
    num_of_runs = 5
for j in range(num_of_runs):
    run_path = set_path + "/" + model_name + "/" + ("run" + str(j))
    changes_per_step, angles_per_step, pos_per_step = sim_data.read_data_from_file(run_path)
    sim_data.create_regular_graphs(run_path, changes_per_step, angles_per_step)
