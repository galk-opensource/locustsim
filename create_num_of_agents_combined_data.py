# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import sys
import sim_data

num_of_agents_path = sys.argv[1]
# num_of_agents_path = "Eden_Experiments/10_agents_same_type"

sim_data.create_avg_graphs(num_of_agents_path)


