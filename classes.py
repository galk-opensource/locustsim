from shapely.geometry import box


class Arena:
    def __init__(self, arena_size):
        self.size = arena_size
        self.polygon = box(0, 0, arena_size[0], arena_size[1])


class Agent:
    def __init__(self, agent_number_global, agent_type, agent_number_within_type, agent_position, agent_polygon,
                 agent_angle, agent_is_walking, agent_world_view, agent_memory, alone_memory):
        self.number_global = agent_number_global
        self.type = agent_type
        self.number_within_type = agent_number_within_type
        self.position = agent_position
        self.polygon = agent_polygon
        self.angle = agent_angle
        self.is_walking = agent_is_walking
        self.world_view = agent_world_view
        self.memory = agent_memory
        self.alone_memory = alone_memory
        self.state = ""


class Type:
    def __init__(self, type_number, type_name, type_class):
        self.number = type_number
        self.name = type_name
        self.type_class = type_class


class Simulation:
    def __init__(self, length, agents_amount_global, steps_amount, agents_rtree, agents_polygons,
                 agents_visual_fields):
        self.length = length
        self.agents_amount_global = agents_amount_global
        self.steps_amount = steps_amount
        self.rtree = agents_rtree
        self.polygons = agents_polygons
        self.visual_fields = agents_visual_fields
