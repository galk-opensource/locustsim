# Eden's branch
import initiation
import sys
import os


num_of_agents = int(sys.argv[1])
path = sys.argv[2]

arena = initiation.create_arena()
pos, angles = initiation.rand_init_pos_and_angles(num_of_agents, arena)
initiation.save_pos_and_angles_to_file(path, pos, angles)
