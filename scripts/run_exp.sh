#!/bin/bash

exp_dir="Eden_Experiments-without_myself"

declare -a models=("daniel's model" "handles crowded & lonely" "always align" "handles only crowded" "handles only lonely")

for num_of_agents in 25
do
	for set in {1..10}
	do
		set_path="../${exp_dir}/${num_of_agents}_agents_same_type/set${set}"
		for model_name in "${models[@]}"
		do
			echo ${set_name}
			echo "${model_name}"
			python3 ../run_eden_exp.py ${num_of_agents} ${set_path} "${model_name}"
		done
	done
done
