#!/bin/bash

# create all folders
exp_dir="Eden_Experiments"

declare -a models=("daniel's model" "handles crowded & lonely" "always align" "handles only crowded" "handles only lonely")

# mkdir "../${exp_dir}"
cd "../${exp_dir}"
for num_of_agents in 10 15 20
do
	# mkdir "${num_of_agents}_agents_same_type"
	cd  "${num_of_agents}_agents_same_type"
	mkdir "avg_data"
	cd "avg_data"
	for model_name in "${models[@]}"
	do
		mkdir "${model_name}"
	done
	cd ..
done
