#!/bin/bash

./create_folders.sh
./rand_init_pos_and_angles.sh
./run_exp.sh
./create_regular_graphs.sh
./create_set_combined_graphs.sh
./create_num_of_agents_combined_graphs.sh
