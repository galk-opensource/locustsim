#!/bin/bash

# create all folders
exp_dir="Eden_Experiments-without_myself"

declare -a models=("daniel's model" "handles crowded & lonely" "always align" "handles only crowded" "handles only lonely")

# mkdir "../${exp_dir}"
cd "../${exp_dir}"
for num_of_agents in 25
do
	mkdir "${num_of_agents}_agents_same_type"
	cd  "${num_of_agents}_agents_same_type"
	for set in {1..10}
	do
		mkdir "set${set}"
		cd "set${set}"
		for model_name in "${models[@]}"
		do
			mkdir "${model_name}"
		done
		cd ..
	done
	cd ..
done
