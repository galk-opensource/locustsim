#!/bin/bash

exp_dir="Eden_Experiments"

declare -a models=("daniel's model" "handles crowded & lonely" "always align" "handles only crowded" "handles only lonely")

for num_of_agents in 10 15 20
do
	for set in {1..10}
	do
		set_path="../${exp_dir}/${num_of_agents}_agents_same_type/set${set}"
		echo "${set_path}"
		python3 ../create_set_combined_data_variance.py ${set_path}
		python3 ../create_set_combined_size_of_change.py ${set_path}
	done
done
