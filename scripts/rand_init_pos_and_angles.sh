#!/bin/bash

exp_dir="Eden_Experiments-without_myself"

declare -a models=("daniel's model" "handles crowded & lonely" "always align" "handles only crowded" "handles only lonely")

for num_of_agents in 25
do
	for set in {1..10}
	do
		path="../${exp_dir}/${num_of_agents}_agents_same_type/set${set}"
		echo "${path}"
		python3 ../rand_init_pos_and_angles.py ${num_of_agents} ${path}
	done
done
