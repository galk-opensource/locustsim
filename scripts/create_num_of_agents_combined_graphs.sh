#!/bin/bash

exp_dir="Eden_Experiments"


for num_of_agents in 10 15 20
do
	num_of_agents_path="../${exp_dir}/${num_of_agents}_agents_same_type"
	echo "${num_of_agents_path}"
	python3 ../create_num_of_agents_combined_data.py ${num_of_agents_path}
	python3 ../create_num_of_agents_combined_variance.py ${num_of_agents_path}
	python3 ../create_num_of_agents_combined_size_of_change.py ${num_of_agents_path}
done
