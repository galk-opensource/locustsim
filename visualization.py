import matplotlib.pyplot as plt


def visualization(axs, simulation, arena_size):
    all_polygons = simulation.polygons
    all_visual_fields = simulation.visual_fields  # in any step updates it (go to simulate.py 24)
    for visual_field in all_visual_fields:
        for sub_visual_field in visual_field:
            if sub_visual_field.geom_type == 'MultiPolygon':
                for sub_sub_visual_field in sub_visual_field:
                    xsys = sub_sub_visual_field.exterior.xy
                    axs.fill(xsys[0], xsys[1], alpha=0.1, fc='g', ec='none')
            else:
                xsys = sub_visual_field.exterior.xy
                axs.fill(xsys[0], xsys[1], alpha=0.1, fc='g', ec='none')
    for polygon in all_polygons:
        for sub_polygon in polygon:
            xsys = sub_polygon.exterior.xy
            axs.fill(xsys[0], xsys[1], alpha=0.5, fc='b', ec='none')
    plt.xlim(0, arena_size[0])
    plt.ylim(0, arena_size[1])
    plt.pause(0.0001)

    return axs
    # plt.show()

