# Pythono3 code to rename multiple
# files in a directory or folder

# importing os module
import os


# Function to rename multiple files
def main():
    i = 1

    for filename in os.listdir("/Users/knebel/Documents/PostDoc/TwoDimModel/model7/5-100_animals_1000_steps-lowFixedProb2stop_alone_memory/"):
        if filename[-1] == 'v':
            ending = 'csv'
        elif filename[-1] == 't':
            ending = 'txt'
        dst = "exp" + str(i+1) + '.' + ending
        src = '/Users/knebel/Documents/PostDoc/TwoDimModel/model7/5-100_animals_1000_steps-lowFixedProb2stop_alone_memory/exp' + str(i) + '.' + ending
        dst = '/Users/knebel/Documents/PostDoc/TwoDimModel/model7/5-100_animals_1000_steps-lowFixedProb2stop_alone_memory2/' + dst

        # rename() function will
        # rename all the files
        os.rename(src, dst)
        i += 1


# Driver Code
if __name__ == '__main__':
    # Calling main() function
    main()