# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import numpy as np


def save_data_to_file(path, changes_per_step, angles_per_step, pos_per_step):
    csvfile = open((path + '/exp_data.csv'), 'w', newline='')
    obj = csv.writer(csvfile)
    obj.writerows([changes_per_step, angles_per_step,pos_per_step])
    csvfile.close()


def read_data_from_file(path):
    csvfile = open((path + '/exp_data.csv'), 'r', newline='')
    obj = csv.reader(csvfile)
    changes_per_step = next(obj)
    changes_per_step = list(map(int, changes_per_step))
    angles_per_step = next(obj)
    for i in range(len(angles_per_step)):
        angles_per_step[i] = ast.literal_eval(angles_per_step[i])
        angles_per_step[i] = list(map(float, angles_per_step[i]))
    pos_per_step = next(obj)
    for step in range(len(pos_per_step)):
        pos_per_step[step] = pos_per_step[step].replace("[[", "[")
        pos_per_step[step] = pos_per_step[step].replace("]]", "]")
        pos_per_step[step] = pos_per_step[step].replace("[array(", "")
        pos_per_step[step] = pos_per_step[step].replace("array(", "")
        pos_per_step[step] = pos_per_step[step].replace(")]", "")
        pos_per_step[step] = pos_per_step[step].replace(")", "")
        pos_per_step[step] = pos_per_step[step].replace(" ", "")
        pos_per_step[step] = pos_per_step[step].replace("],", "]|")
        pos_per_step[step] = pos_per_step[step].split("|")
        for i in range(len(pos_per_step[step])):
            pos_per_step[step][i] = ast.literal_eval(pos_per_step[step][i])
        # pos_per_step[i] = list(map(float, pos_per_step[i]))
    csvfile.close()
    return changes_per_step, angles_per_step, pos_per_step


def create_regular_graphs(path, changes_per_step, angles_per_step):
    normalization = True
    num_of_steps = len(changes_per_step)
    steps = list(range(num_of_steps))
    num_of_agents = len(angles_per_step[0])

    if normalization:
        for i in range(num_of_steps):
            changes_per_step[i] = changes_per_step[i]/num_of_agents

    angle_changes_Data = {'Step': steps,
            'Angle_changes': changes_per_step
            }

    angle_changes_df = DataFrame(angle_changes_Data, columns=['Step', 'Angle_changes'])
    angle_changes_df.plot(x='Step', y='Angle_changes', kind='line')

    # plt.show()
    plt.savefig((path + "/angle_changes"))
    print()

    #---------------------------------------------#

    variance_per_step = angles_per_step_to_variance_per_step(angles_per_step)

    angle_variance_Data = {'Step': steps,
                          'Angle_variance': variance_per_step
                          }

    angle_variance_df = DataFrame(angle_variance_Data, columns=['Step', 'Angle_variance'])
    angle_variance_df.plot(x='Step', y='Angle_variance', kind='line')

    # plt.show()
    plt.savefig((path + "/angle_variance"))
    print()

    # ---------------------------------------------#


    size_of_change_per_step = angles_per_step_to_size_of_change_per_step(angles_per_step)

    num_of_steps = len(size_of_change_per_step)
    steps = list(range(num_of_steps))

    angle_size_of_change_Data = {'Step': steps,
                           'Size_of_change(degrees)': size_of_change_per_step
                           }

    angle_size_of_change_df = DataFrame(angle_size_of_change_Data, columns=['Step', 'Size_of_change(degrees)'])
    angle_size_of_change_df.plot(x='Step', y='Size_of_change(degrees)', kind='line')

    # plt.show()
    plt.savefig((path + "/size_of_change(degrees)"))
    print()

    # ---------------------------------------------#

    angles_last_step = angles_per_step[-1]
    angles_last_step = ['%.2f' % elem for elem in angles_last_step]
    diff_angles = list(dict.fromkeys(angles_last_step))
    diff_angles.sort()
    # angles_degree = []
    angles_count = []
    for angle in diff_angles:
        # angles_degree.append(angle * 180 / math.pi)
        angles_count.append(angles_last_step.count(angle))

    # degrees_diff_angles = list(dict.fromkeys(angles_degree))
    # if not len(degrees_diff_angles) == len(diff_angles):
    #     print("BUGGGGGG")

    angles_last_step_Data = {'Angles': diff_angles,
                          'Angles_last_step': angles_count
                          }

    angles_last_step_df = DataFrame(angles_last_step_Data, columns=['Angles', 'Angles_last_step'])
    angles_last_step_df.plot(x='Angles', y='Angles_last_step', kind='bar', figsize=(8,8))

    # plt.show()
    plt.savefig((path + "/angles_last_step"))
    print()


def create_combined_graphs(path, data_name, models_to_compare, all_data):
    steps = list(range(len(all_data[models_to_compare[0]])))

    combined_dic = {}
    combined_dic['step'] = steps
    for model_name in models_to_compare:
        combined_dic[model_name] = all_data[model_name]
    df = DataFrame(combined_dic)

    ax = plt.gca()

    # colors = ['']

    for model_name in models_to_compare:
        df.plot(kind='line', x='step', y=model_name, ax=ax, figsize=(8,8))
        # df.plot(kind='bar', x='step', y=model_name, ax=ax, figsize=(8,8))

    plt.ylabel(data_name)
    plt.savefig((path + "/combined-" + data_name))
    #plt.show()
    plt.gca().clear()
    print()


def angles_distance(angle1, angle2):
    unit1 = math.degrees(angle1)
    unit2 = math.degrees(angle2)
    phi = abs(unit2-unit1) % 360
    sign = 1
    # used to calculate sign
    if not ((unit1-unit2 >= 0 and unit1-unit2 <= 180) or (
            unit1-unit2 <= -180 and unit1-unit2 >= -360)):
        sign = -1
    if phi > 180:
        result = 360-phi
    else:
        result = phi

    # return math.radians(result)
    return result


def angles_per_step_to_variance_per_step(angles_per_step):
    variance_per_step = []

    for angles in angles_per_step:
        mean_ang = circmean(angles)
        variance = circvar(angles)
        variance_per_step.append(variance)

    return variance_per_step


def angles_per_step_to_size_of_change_per_step(angles_per_step):
    num_of_agents = len(angles_per_step[0])
    size_of_change_per_step = []
    for i in range(len(angles_per_step)):
        if i > 0:
            size_of_change = 0.0
            old_angles = angles_per_step[i-1]
            new_angles = angles_per_step[i]
            for j in range(len(old_angles)):
                size_of_change = size_of_change + angles_distance(old_angles[j], new_angles[j])
            size_of_change  = size_of_change /num_of_agents # TODO: add normalization
            size_of_change_per_step.append(size_of_change)
    return size_of_change_per_step


def create_set_variance_combined_graph(set_path):
    data_name = 'variance'
    all_data = {}
    # models_to_compare = ["daniel's model", "handles crowded & lonely", "always align","handles only crowded","handles only lonely"]
    models_to_compare = ["handles crowded & lonely", "always align", "handles only crowded",
                         "handles only lonely"]
    for model_name in models_to_compare:
        run_path = set_path + "/" + model_name + "/run0"
        changes_per_step, angles_per_step, pos_per_step = read_data_from_file(run_path)
        variance_per_step = angles_per_step_to_variance_per_step(angles_per_step)
        all_data[model_name] = variance_per_step

    create_combined_graphs(set_path, data_name, models_to_compare, all_data)


def create_set_size_of_change_combined_graphs(set_path):
    data_name = 'size_of_change(degrees)'
    all_data = {}
    # models_to_compare = ["daniel's model", "handles crowded & lonely", "always align","handles only crowded","handles only lonely"]
    models_to_compare = ["handles crowded & lonely", "always align", "handles only crowded",
                         "handles only lonely"]
    for model_name in models_to_compare:
        run_path = set_path + "/" + model_name + "/run0"
        changes_per_step, angles_per_step, pos_per_step = read_data_from_file(run_path)
        size_of_change_per_step = angles_per_step_to_size_of_change_per_step(angles_per_step)
        all_data[model_name] = size_of_change_per_step

    create_combined_graphs(set_path, data_name, models_to_compare, all_data)


def create_avg_data_graph_per_step(path, avg_variance_per_step, avg_size_of_change_per_step):
    steps = list(range(len(avg_variance_per_step)))

    angle_changes_Data = {'Step': steps,
                          'Size_of_change(degrees)': avg_size_of_change_per_step
                          }

    angle_changes_df = DataFrame(angle_changes_Data, columns=['Step', 'Size_of_change(degrees)'])
    angle_changes_df.plot(x='Step', y='Size_of_change(degrees)', kind='line')

    # plt.show()
    plt.savefig((path + "/size_of_change(degrees)"))
    print()

    # ---------------------------------------------#

    angle_variance_Data = {'Step': steps,
                           'Avg_angle_variance': avg_variance_per_step
                           }

    angle_variance_df = DataFrame(angle_variance_Data, columns=['Step', 'Avg_angle_variance'])
    angle_variance_df.plot(x='Step', y='Avg_angle_variance', kind='line')

    # plt.show()
    plt.savefig((path + "/avg_angle_variance"))
    print()


def save_avg_data_to_file(path, avg_variance_per_step, avg_size_of_change_per_step):
    csvfile = open((path + '/avg_data.csv'), 'w', newline='')
    obj = csv.writer(csvfile)
    obj.writerows([avg_variance_per_step, avg_size_of_change_per_step])
    csvfile.close()


def read_avg_data(path):
    csvfile = open((path + '/avg_data.csv'), 'r', newline='')
    obj = csv.reader(csvfile)
    avg_variance_per_step = next(obj)
    avg_variance_per_step = list(map(float, avg_variance_per_step))
    avg_size_of_change_per_step = next(obj)
    avg_size_of_change_per_step = list(map(float, avg_size_of_change_per_step))
    csvfile.close()

    return avg_variance_per_step, avg_size_of_change_per_step


def create_avg_graphs(num_of_agents_path):
    num_of_sets = 10
    models_to_compare = ["daniel's model", "handles crowded & lonely", "always align", "handles only crowded",
                         "handles only lonely"]
    # models_to_compare = ["daniel's model"]

    avg_path = num_of_agents_path + "/avg_data"
    # os.mkdir(avg_path)
    # print("create avg folder")
    for model_name in models_to_compare:
        variance_per_step_array = []
        size_of_change_per_step_array = []
        for i in range(num_of_sets):
            set_path = num_of_agents_path + "/set" + str(i+1)
            run_path = set_path + "/" + model_name + "/run0"
            changes_per_step, angles_per_step, pos_per_step = read_data_from_file(run_path)
            variance_per_step_array.append(angles_per_step_to_variance_per_step(angles_per_step))
            size_of_change_per_step_array.append(angles_per_step_to_size_of_change_per_step(angles_per_step))
        num_of_steps = len(size_of_change_per_step_array[0])
        avg_variance_per_step = list(np.zeros(num_of_steps))
        avg_size_of_change_per_step = list(np.zeros(num_of_steps))
        for step in range(num_of_steps):
            for i in range(num_of_sets):
                avg_variance_per_step[step] = avg_variance_per_step[step] + variance_per_step_array[i][step]
                avg_size_of_change_per_step[step] = avg_size_of_change_per_step[step] + size_of_change_per_step_array[i][step]
            avg_variance_per_step[step] = avg_variance_per_step[step] / num_of_sets
            avg_size_of_change_per_step[step] = avg_size_of_change_per_step[step] / num_of_sets
        avg_model_path = avg_path + "/" + model_name
        # os.mkdir(avg_model_path)
        save_avg_data_to_file(avg_model_path, avg_variance_per_step, avg_size_of_change_per_step)
        create_avg_data_graph_per_step(avg_model_path, avg_variance_per_step, avg_size_of_change_per_step)


def create_avg_variance_combined_graph(num_of_agents_path):
    models_to_compare = ["handles crowded & lonely", "always align", "handles only crowded",
                         "handles only lonely"]
    #models_to_compare = ["daniel's model"]
    all_data = {}
    avg_path = num_of_agents_path + "/avg_data"
    for model_name in models_to_compare:
        avg_model_path =  avg_path + "/" + model_name
        avg_variance_per_step, avg_size_of_change_per_step = read_avg_data(avg_model_path)
        all_data[model_name] = avg_variance_per_step

    create_combined_graphs(avg_path, "avg_variance", models_to_compare, all_data)


def create_avg_size_of_change_combined_graphs(num_of_agents_path):
    models_to_compare = ["handles crowded & lonely", "always align", "handles only crowded",
                          "handles only lonely"]
    # models_to_compare = ["daniel's model"]
    all_data = {}
    avg_path = num_of_agents_path + "/avg_data"
    for model_name in models_to_compare:
        avg_model_path = avg_path + "/" + model_name
        avg_variance_per_step, avg_size_of_change_per_step = read_avg_data(avg_model_path)
        avg_size_of_change_per_step = avg_size_of_change_per_step[1:]  # because always align first value destroys the graphs
        #avg_size_of_change_per_step.insert(0,0)
        all_data[model_name] = avg_size_of_change_per_step

    create_combined_graphs(avg_path, "avg_size_of_change(degrees)", models_to_compare, all_data)