import initiation
import simulate
from datetime import datetime
import os

folder = '/Users/knebel/Desktop/exp4/Walk0804fixedStop02_memory5'
# os.mkdir(folder)
initial_animals_amount = 2
max_animals_amount = 42
jumps_in_animal_amount = 2
number_of_reps = 20
g = 0

for number_of_animals in list(range(initial_animals_amount, max_animals_amount, jumps_in_animal_amount)):
    for i in range(number_of_reps):
        g += 1
        arena = initiation.create_arena()
        types = initiation.create_types()
        types[0].type_class.amount = number_of_animals
        simulation = initiation.create_simulation(types)
        agents, simulation = initiation.create_agents(arena, types, simulation)
        name = datetime.now()
        name = 'exp' + str(g)
        file = open(folder + '/' + name + '.txt', 'w')
        for attr in dir(types[0].type_class):
            if hasattr(types[0].type_class, attr):
                file.write(folder + "/type_class.%s = %s \n" % (attr, getattr(types[0].type_class, attr)))  # data about type
        file.close()
        txtFile = ''
        csvFile = name + '.csv'
        agents, simulation = simulate.create_simulation(simulation, arena, types, agents, 0, folder + '/' + csvFile)

