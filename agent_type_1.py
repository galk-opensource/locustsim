import random
import math
from functions import calculate_corners, create_rtree, check_collisions, create_visual_field, weighted_circmean, \
    circmean_r_length, correct4pbc, calculate_concealment, create_pbc_polygons
from classes import Agent
import numpy as np
from scipy.stats import circvar, circmean


#----------------------------------------------#
# base on three zones model - attraction anf repellation



def create_type1_class():
    """input"""

    agents_amount = 0
    agent_size = [0.1, 0.4]
    agent_speed = 0.1
    agent_prob2walk = 1
    agent_prob2stop = 0
    angle_visual_range = 1
    angle_r1 = 0.3  # radius of too close
    angle_r2 = 0.6  # radius of ideal
    agent_memory_length = 5
    agent_alone_memory_length = 5  # if 0 always 0.4 (only now)
    agent_angle_inertia = 0.7  # was 0.2
    agent_angle_surrounding = 0.3  # was 0.3
    agent_angle_memory = 0  # was 0.2
    prob2walk_alone = 0.8
    """code"""
    agent_center2edge = [0.5 * agent_size[0], 0.5 * agent_size[1]]
    agent_alpha = math.atan(agent_center2edge[0] / agent_center2edge[1])
    agent_dimensions = {"size": agent_size, "alpha": agent_alpha, "center2edge": agent_center2edge}
    type1 = Type1(agents_amount, agent_dimensions, agent_speed, agent_prob2walk, agent_prob2stop, angle_visual_range,
                  agent_memory_length, agent_alone_memory_length, agent_angle_inertia, agent_angle_surrounding,
                  agent_angle_memory, prob2walk_alone)
    return type1


class Type1:
    """code"""

    def __init__(self, agents_amount, agent_dimensions, agent_speed, agent_prob2walk, agent_prob2stop,
                 angle_visual_range, agent_memory_length, agent_alone_memory_length, agent_angle_inertia,
                 agent_angle_surrounding, agent_angle_memory, prob2walk_alone):
        self.amount = agents_amount
        self.dimensions = agent_dimensions
        self.speed = agent_speed
        self.prob2walk = agent_prob2walk
        self.prob2stop = agent_prob2stop
        self.visual_range = angle_visual_range
        self.memory_length = agent_memory_length
        self.alone_memory_length = agent_alone_memory_length
        self.angle_inertia = agent_angle_inertia
        self.angle_surrounding = agent_angle_surrounding
        self.angle_memory = agent_angle_memory
        self.angle_error = 1 - agent_angle_memory - agent_angle_surrounding - agent_angle_inertia
        self.prob2walk_alone = prob2walk_alone
        # need to add this in the c'tor
        self.angle_r1 = 0.3  # radius of too close
        self.angle_r2 = 0.6  # radius of ideal


def create_type1_agents(arena, agents_polygons, type1, agent_number_global, agent_number_within_type):  # only one agent
    """code"""
    intersects = True
    agent_is_walking = random.randint(0, 1)
    while intersects:
        agent_position = [random.uniform(0, arena.size[0]), random.uniform(0, arena.size[1])]
        agent_angle = random.uniform(math.pi / 180, 2 * math.pi)

        agent_corners = calculate_corners(type1.dimensions['alpha'], agent_angle,
                                          agent_position, type1.dimensions['center2edge'],
                                          arena.size)

        agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
        intersects = check_collisions(agents_polygons[:agent_number_global], agent_polygon, agent_number_global)
    agent_type1 = Agent(agent_number_global, 1, agent_number_within_type, agent_position,
                        agent_polygon, agent_angle, agent_is_walking, [None], [None] * (type1.memory_length + 1),
                        [None] * (type1.alone_memory_length + 1))
    agent_type1.state = "program"
    return agent_type1


def create_type1_agents_fix(arena, agents_polygons, type1, agent_number_global, agent_number_within_type, pos, angle):  # only one agent
    """code"""
    intersects = True
    agent_is_walking = random.randint(0, 1)
    agent_position = pos
    agent_angle = angle

    agent_corners = calculate_corners(type1.dimensions['alpha'], agent_angle,
                                      agent_position, type1.dimensions['center2edge'],
                                      arena.size)

    agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
    agent_type1 = Agent(agent_number_global, 1, agent_number_within_type, agent_position,
                        agent_polygon, agent_angle, agent_is_walking, [None], [None] * (type1.memory_length + 1),
                        [None] * (type1.alone_memory_length + 1))
    return agent_type1


def create_type1_worldview(agent_number_global, agent_position, agents, arena, agents_polygons, type1):  # in every step
    agents_rtree = create_rtree(agents_polygons, agent_number_global)
    visual_field_not_concealed = create_visual_field(agent_position, type1.visual_range, arena) # without hidding
    visual_field = calculate_concealment(agents[agent_number_global], agents, visual_field_not_concealed,
                                         agents_polygons, type1, arena)
    intersecting_agents = []
    i = -1
    for sub_visual_field in visual_field:
        i += 1
        if agents_rtree.query(sub_visual_field):
            for other_agent in agents:
                if other_agent.number_global == agent_number_global:
                    continue
                else:
                    for other_agent_sub_polygon in other_agent.polygon:
                        if sub_visual_field.intersects(other_agent_sub_polygon):
                            intersecting_agents.append(
                                {"number_global": other_agent.number_global, "is_walking": other_agent.is_walking,
                                 "angle": other_agent.angle})
    return intersecting_agents, visual_field  #

def find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena, agents_polygons, type1, radius):
    agents_rtree = create_rtree(agents_polygons, agent_number_global)
    visual_field_not_concealed = create_visual_field(agent_position, radius, arena)  # without hidding
    visual_field = calculate_concealment(agents[agent_number_global], agents, visual_field_not_concealed,
                                         agents_polygons, type1, arena)
    intersecting_agents = []
    i = -1
    for sub_visual_field in visual_field:
        i += 1
        if agents_rtree.query(sub_visual_field):
            for other_agent in agents:
                if other_agent.number_global == agent_number_global:
                    continue
                else:
                    for other_agent_sub_polygon in other_agent.polygon:
                        if sub_visual_field.intersects(other_agent_sub_polygon):
                            intersecting_agents.append(
                                {"number_global": other_agent.number_global, "is_walking": other_agent.is_walking,
                                 "angle": other_agent.angle})
    return intersecting_agents


def find_intersecting_agents_by_zones(agent_number_global, agent_position, agents, arena, agents_polygons, type1):  # in every step
    agents_within_r1 = find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena, agents_polygons, type1, type1.angle_r1)
    agents_within_r2 = find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena, agents_polygons, type1, type1.angle_r2)
    agents_within_range = find_intersecting_agents_within_radius(agent_number_global, agent_position, agents, arena,
                                                              agents_polygons, type1, type1.visual_range)

    agents_ideal = [item for item in agents_within_r2 if item not in agents_within_r1]
    agents_too_far = [item for item in agents_within_range if item not in agents_within_r2]

    return agents_within_r1, agents_ideal, agents_too_far


def move_type1_agents(agent, agents, arena, agents_polygons, type1):  # loop
    agents_too_close, agents_ideal, agents_too_far = find_intersecting_agents_by_zones(agent.number_global, agent.position, agents, arena,
                                                               agents_polygons,
                                                               type1)

    intersecting_agents, visual_field = create_type1_worldview(agent.number_global, agent.position, agents, arena,
                                                               agents_polygons,
                                                               type1)
    agent_new_angle = agent.angle

    #----------------mdp--------------------------------#
    #---------------------------------------------------#
    if agent.state == "program":

        if agents_too_close:  # there are other agents within r1 (too close --> crowded)
            agent.state = "crowded"
            # handle
            field_angles = []
            if agents_too_close:
                for agent_i in agents_too_far:
                    if agent_i["is_walking"]:
                        field_angles.append((agent_i["angle"]))
            field_mean_angle = circmean(field_angles)
            memory_angles = []
            memory_mean_angle = circmean(memory_angles)
            weights = [type1.angle_inertia, type1.angle_memory, type1.angle_surrounding, type1.angle_error]
            error_angle = random.uniform(math.pi / 180, 2 * math.pi)
            angles = [agent.angle, field_mean_angle, memory_mean_angle, error_angle]
            agent_new_angle = weighted_circmean(weights, angles) + math.pi

        elif not agents_ideal:  # there are NO other agents within r2 (too far --> lonely)
            agent.state = "lonely"
            # need to consider agents_too_far = []
            # handle
            if agents_too_far:
                field_angles = []
                if agents_too_far:
                    for agent_i in agents_too_far:
                        if agent_i["is_walking"]:
                            field_angles.append((agent_i["angle"]))
                field_mean_angle = circmean(field_angles)
                memory_angles = []
                memory_mean_angle = circmean(memory_angles)
                weights = [type1.angle_inertia, type1.angle_memory, type1.angle_surrounding, type1.angle_error]
                error_angle = random.uniform(math.pi / 180, 2 * math.pi)
                angles = [agent.angle, field_mean_angle, memory_mean_angle, error_angle]
                agent_new_angle = weighted_circmean(weights, angles)

        else:
            # keep moving?
            agent.state = "program"  # dummy
    #---------------------------------------------------#
    elif agent.state == "crowded":
        # change angle and return to "program"?
        agent.state = "program"
    #---------------------------------------------------#
    else:  # agent.state ==lonely
        # change angle and return to "program"?
        agent.state = "program"
    #---------------------------------------------------#



    agent_new_position = [agent.position[0] + math.cos(agent_new_angle) * type1.speed,
                          agent.position[1] + math.sin(agent_new_angle) * type1.speed]
    agent_new_position_pbc = np.squeeze(correct4pbc([agent_new_position], arena.size)[-1])
    agent_corners = calculate_corners(type1.dimensions['alpha'], agent_new_angle,
                                      agent_new_position_pbc,
                                      type1.dimensions['center2edge'], arena.size)
    agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)

    agent.angle = agent_new_angle
    agent.position = agent_new_position_pbc
    agent.polygon = agent_polygon

    return agent, visual_field
