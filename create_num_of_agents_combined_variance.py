# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import sys
import sim_data

num_of_agents_path = sys.argv[1]

sim_data.create_avg_variance_combined_graph(num_of_agents_path)


