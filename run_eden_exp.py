# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import sys
import sim_data


# folders structure:
#
#   Eden_Experiments
#       type1
#           set1 (of angles and positions)
#               model_x
#                   run1


num_of_agents = int(sys.argv[1])
set_path = sys.argv[2]
model_name = sys.argv[3]

# num_of_agents = 10
# set_path = "Eden_Experiments/10_agents_same_type/set1"
# model_name = "handles crowded & lonely"

arena = initiation.create_arena()
pos, angles = initiation.read_init_pos_and_angles(set_path)

        # type 0 - Daniel's model
        # type 2 - handles both crowded and lonely
        # type 3 - align all the time
        # type 4 - handles only crowded
        # type 5 - handles only lonely

agents_sets = [["daniel's model", [num_of_agents, 0, 0, 0, 0, 0]],
                       ["handles crowded & lonely", [0, 0, num_of_agents, 0, 0, 0]],
                       ["always align", [0, 0, 0, num_of_agents, 0, 0]],
                       ["handles only crowded", [0, 0, 0, 0, num_of_agents, 0]],
                       ["handles only lonely", [0, 0, 0, 0, 0, num_of_agents]]]

for agents_set in agents_sets:
    if model_name == agents_set[0]:
        agents_amount = agents_set[1]
        break

num_of_runs = 1
if model_name == "daniel's model":
    num_of_runs = 5
for j in range(num_of_runs):
    run_path = set_path + "/" + model_name + "/" + ("run" + str(j))
    os.mkdir(run_path)
    types = initiation.create_types_with_amounts(agents_amount)
    simulation = initiation.create_simulation(types)
    agents, simulation = initiation.create_agents_with_fix_vec(arena, types, simulation, pos, angles)
    fileName = 'fileName.csv'
    agents, simulation, changes_per_step, angles_per_step, pos_per_step = \
        simulate.create_exp_simulation(simulation, arena, types, agents, 0, fileName, pos, angles)
    sim_data.save_data_to_file(run_path, changes_per_step, angles_per_step, pos_per_step)




