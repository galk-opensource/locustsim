import math
import numpy as np
from shapely.strtree import STRtree
from shapely.geometry import Polygon, Point, LineString
from shapely.ops import split


def calculate_corners(alpha, angle, position, center2edge, arena_size):
    half_diag = math.sqrt(center2edge[0] ** 2 + center2edge[1] ** 2)
    corner1_raw = [position[0] + half_diag * math.cos(alpha + angle),  # = Upper right and CW
                   position[1] + half_diag * math.sin(alpha + angle)]
    corner2_raw = [position[0] + half_diag * math.cos(- alpha + angle),
                   position[1] + half_diag * math.sin(- alpha + angle)]
    corner3_raw = [position[0] + half_diag * math.cos(math.pi + alpha + angle),
                   position[1] + half_diag * math.sin(math.pi + alpha + angle)]
    corner4_raw = [position[0] + half_diag * math.cos(math.pi - alpha + angle),
                   position[1] + half_diag * math.sin(math.pi - alpha + angle)]
    corners_raw = [corner1_raw, corner2_raw, corner3_raw, corner4_raw]
    # print("call from calculate_corners")
    corners_pbc = correct4pbc(corners_raw, arena_size)
    return corners_pbc


def correct4pbc(points, arena_size):
    xs = np.array([item[0] for item in points])
    ys = np.array([item[1] for item in points])
    flag_x = 0
    flag_y = 0
    if any(xs >= arena_size[0]):
        new_xs = xs - arena_size[0]
        flag_x += 1
    elif any(xs < 0):
        new_xs = xs + arena_size[0]
        flag_x += 1
    else:
        new_xs = xs
    if any(ys >= arena_size[1]):
        new_ys = ys - arena_size[1]
        flag_y += 1
    elif any(ys < 0):
        new_ys = ys + arena_size[1]
        flag_y += 1
    else:
        new_ys = ys
    if not any([flag_x, flag_y]):  # didn't change
        new_points = [np.column_stack((xs, ys))]
    elif not all([flag_x, flag_y]):
        new_points = [np.column_stack((xs, ys)), np.column_stack((new_xs, new_ys))]  # only one changed
    else:  # both changed
        new_points = [np.column_stack((xs, ys)), np.column_stack((new_xs, ys)), np.column_stack((xs, new_ys)),
                      np.column_stack((new_xs, new_ys))]
    new_points = np.array(new_points)
    return new_points


def create_pbc_polygons(points, arena_polygon):
    if points.shape[0] == 1:
        raw_polygon = Polygon(points[0])
        polygons = [raw_polygon]
    elif points.shape[0] == 2:
        raw_polygon = Polygon(points[0]).intersection(arena_polygon)
        pbc_polygon = Polygon(points[1]).intersection(arena_polygon)
        polygons = [raw_polygon, pbc_polygon]
    elif points.shape[0] == 4:
        raw_polygon = Polygon(points[0]).intersection(arena_polygon)
        pbc_polygon1 = Polygon(points[1]).intersection(arena_polygon)
        pbc_polygon2 = Polygon(points[2]).intersection(arena_polygon)
        pbc_polygon3 = Polygon(points[3]).intersection(arena_polygon)
        polygons = [raw_polygon, pbc_polygon1, pbc_polygon2, pbc_polygon3]
    polygons_cleaned = []
    areas = []
    for polygon, polygon_index in zip(polygons, range(len(polygons))):
        if not polygon.geom_type == 'GeometryCollection':
            polygons_cleaned.append(polygon)
            areas.append(-polygon.area)
    polygons_cleaned_sorted = list(zip(*sorted(zip(areas, polygons_cleaned))))[1]
    return polygons_cleaned_sorted


def create_rtree(agents_polygons, agents2skip):
    agents_polygons_flat = []
    for agent_polygon, agent_number_global in zip(agents_polygons, range(len(agents_polygons))):
        if agents2skip == agent_number_global:
            continue
        else:
            for sub_polygon in agent_polygon:
                agents_polygons_flat.append(sub_polygon)
    agents_rtree = STRtree(agents_polygons_flat)

    return agents_rtree


def check_collisions(agents_polygons, agent_polygon, agent_number_global):
    agents_rtree = create_rtree(agents_polygons, agent_number_global)
    intersections = []
    intersects = True
    for sub_polygons in agent_polygon:
        intersections.append(agents_rtree.query(sub_polygons))
    if not any(intersections):
        intersects = False
    return intersects


def create_visual_field(agent_position, visual_range, arena):
    center = Point(agent_position)
    visual_field = center.buffer(visual_range)
    visual_field_circum_raw = visual_field.boundary.coords
    # print("call from create_visual_field")
    visual_field_circum = correct4pbc(visual_field_circum_raw, arena.size)
    visual_field_polygons = create_pbc_polygons(visual_field_circum, arena.polygon)
    return visual_field_polygons


def calculate_concealment(agent, agents, visual_field, agents_polygons, agent_type, arena):
    agents_rtree = create_rtree(agents_polygons, agent.number_global)
    concealed_sub_visual_field = [sub_visual_field for sub_visual_field in visual_field]
    for sub_visual_field, sub_visual_field_index in zip(visual_field, range(len(visual_field))):
        if agents_rtree.query(sub_visual_field):
            for other_agent in agents:
                if agent.number_global == other_agent.number_global:
                    continue
                else:
                    other_agent_center = other_agent.position
                    other_agent_length = agent_type.dimensions['size'][1]
                    other_agent_angle = other_agent.angle
                    point1 = [other_agent_center[0] + other_agent_length / 2 * math.cos(other_agent_angle),
                              other_agent_center[1] + other_agent_length / 2 * math.sin(other_agent_angle)]
                    point2 = [other_agent_center[0] + other_agent_length / 2 * math.cos(
                        math.pi + other_agent_angle),
                              other_agent_center[1] + other_agent_length / 2 * math.sin(
                                  math.pi + other_agent_angle)]
                    if any([Point(point1).within(concealed_sub_visual_field[sub_visual_field_index]),
                            Point(point2).within(concealed_sub_visual_field[sub_visual_field_index])]):
                        if sub_visual_field_index == 0:
                            agent_position = agent.position
                        elif np.shape(sub_visual_field.exterior)[0] <= 5:
                            continue
                        else:
                            agent_position = circle_center_by_3_points(sub_visual_field.boundary.coords[:-1])
                        visual_range = eval('agent_type.visual_range')  # + 1e-3
                        point1_to_center = Point(agent_position).distance(Point(point1))
                        point2_to_center = Point(agent_position).distance(Point(point2))
                        point1_to_circum = Point([agent_position[0] + visual_range, agent_position[1]]).distance(
                            Point(point1))
                        point2_to_circum = Point([agent_position[0] + visual_range, agent_position[1]]).distance(
                            Point(point2))
                        if point1[1] == agent_position[1]:
                            angle1 = math.pi
                        else:
                            to_acos1 = (point1_to_center ** 2 + visual_range ** 2 - point1_to_circum ** 2) / (
                                        2 * point1_to_center * visual_range)
                            if to_acos1 > 1:
                                to_acos1 = 1
                            elif to_acos1 < -1:
                                to_acos1 = -1
                            angle1 = math.acos(to_acos1)
                            if point1[1] < agent_position[1]:
                                angle1 = -angle1
                        if point2[1] == agent_position[1]:
                            angle2 = math.pi
                        else:
                            to_acos2 = (point2_to_center ** 2 + visual_range ** 2 - point2_to_circum ** 2) / (
                                        2 * point2_to_center * visual_range)
                            if to_acos2 > 1:
                                to_acos2 = 1
                            elif to_acos2 < -1:
                                to_acos2 = -1
                            angle2 = math.acos(to_acos2)
                            if point2[1] < agent_position[1]:
                                angle2 = -angle2
                        line1_circum_point_x = agent_position[0] + math.cos(angle1) * visual_range
                        line1_circum_point_y = agent_position[1] + math.sin(angle1) * visual_range
                        line2_circum_point_x = agent_position[0] + math.cos(angle2) * visual_range
                        line2_circum_point_y = agent_position[1] + math.sin(angle2) * visual_range
                        concealing_points_raw = [[line1_circum_point_x, line1_circum_point_y], point1, point2,
                                                 [line2_circum_point_x, line2_circum_point_y]]
                        concealing_points = correct4pbc(concealing_points_raw, arena.size)
                        concealing_polygon = create_concealed_polygon(concealing_points, sub_visual_field,
                                                                      agent_position)
                        if not concealing_polygon.is_valid:
                            concealing_polygon = concealing_polygon.buffer(0)
                        if not concealed_sub_visual_field[sub_visual_field_index].is_valid:
                            concealed_sub_visual_field[sub_visual_field_index] = concealed_sub_visual_field[sub_visual_field_index].buffer(0)
                        concealed_sub_visual_field_temp = concealed_sub_visual_field[
                            sub_visual_field_index].difference(concealing_polygon)
                        # concealed_sub_visual_field_temp = concealed_sub_visual_field[
                        #     sub_visual_field_index].buffer(0).difference(concealing_polygon)
                        if concealed_sub_visual_field_temp.geom_type == 'GeometryCollection' and len(
                                concealed_sub_visual_field_temp) != 0:
                            # concealed_sub_visual_field[sub_visual_field_index] = \
                            #  concealed_sub_visual_field[sub_visual_field_index].buffer(0).difference(concealing_polygon)[1]
                            concealed_sub_visual_field[sub_visual_field_index] = \
                                concealed_sub_visual_field[sub_visual_field_index].difference(concealing_polygon)[1]
                            if concealed_sub_visual_field_temp.geom_type == 'GeometryCollection':
                                concealed_sub_visual_field[sub_visual_field_index] = \
                                concealed_sub_visual_field_temp[-1]
                            else:
                                concealed_sub_visual_field[sub_visual_field_index] = \
                                    concealed_sub_visual_field_temp
                        else:
                            concealed_sub_visual_field[sub_visual_field_index] = concealed_sub_visual_field_temp
                        if sub_visual_field_index == 0 and np.shape(concealing_points)[0] > 1:
                            i = 0
                            for sub_visual_field2 in visual_field[1:]:
                                i += 1
                                concealing_polygon = create_concealed_polygon(concealing_points, sub_visual_field2,
                                                                              agent_position)
                                # concealed_sub_visual_field_temp = concealed_sub_visual_field[
                                #     sub_visual_field_index + i].buffer(0).difference(concealing_polygon)
                                if not concealed_sub_visual_field[sub_visual_field_index].is_valid:
                                    concealed_sub_visual_field[sub_visual_field_index] = concealed_sub_visual_field[
                                        sub_visual_field_index].buffer(0)
                                if not concealing_polygon.is_valid:
                                    concealing_polygon = concealing_polygon.buffer(0)
                                concealed_sub_visual_field_temp = concealed_sub_visual_field[
                                    sub_visual_field_index + i].difference(concealing_polygon)
                                if concealed_sub_visual_field_temp.geom_type == 'GeometryCollection' and len(
                                        concealed_sub_visual_field_temp):
                                    # concealed_sub_visual_field_temp = \
                                    # concealed_sub_visual_field[sub_visual_field_index + i].buffer(0).difference(
                                    #     concealing_polygon)[1]
                                    concealed_sub_visual_field[sub_visual_field_index + i].difference(concealing_polygon)[1]
                                    if concealed_sub_visual_field_temp.geom_type == 'GeometryCollection':
                                        concealed_sub_visual_field[sub_visual_field_index + i] = concealed_sub_visual_field_temp[-1]
                                    else:
                                        concealed_sub_visual_field[sub_visual_field_index + i] = \
                                        concealed_sub_visual_field_temp
                                else:
                                    concealed_sub_visual_field[
                                        sub_visual_field_index + i] = concealed_sub_visual_field_temp

    return concealed_sub_visual_field


def weighted_circmean(weights, angles):  # problem
    x = y = 0
    nan_values = np.squeeze(np.argwhere(np.isnan(angles)))
    if nan_values.size != 0:
        weights = np.delete(weights, nan_values)
        angles = np.delete(angles, nan_values)
        weights[-1] = 1 - np.sum(weights[:-1])
    for angle, weight in zip(angles, weights):
        x += math.cos(angle) * weight
        y += math.sin(angle) * weight
    mean_weighted_angle = math.atan2(y, x)
    return mean_weighted_angle


def circmean_r_length(angles):
    angles = np.array(angles)
    if len(angles) == 0:
        r_length = 1
    else:
        r_length = np.abs(np.sum(np.exp(1j * angles))) / len(angles)
    return r_length


def circle_center_by_3_points(points):
    """
    Returns the center and radius of the circle passing the given 3 points.
    In case the 3 points form a line, returns (None, infinity).
    """
    p1 = points[0]
    p2 = points[round(len(points) / 2)]
    p3 = points[-2]
    temp = p2[0] * p2[0] + p2[1] * p2[1]
    bc = (p1[0] * p1[0] + p1[1] * p1[1] - temp) / 2
    cd = (temp - p3[0] * p3[0] - p3[1] * p3[1]) / 2
    det = (p1[0] - p2[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p2[1])
    # Center of circle
    cx = (bc * (p2[1] - p3[1]) - cd * (p1[1] - p2[1])) / det
    cy = ((p1[0] - p2[0]) * cd - (p2[0] - p3[0]) * bc) / det
    return [cx, cy]


def create_concealed_polygon(concealing_points, sub_visual_field, agent_position):
    for concealing_points_pbc in concealing_points:
        full_concealing_polygon = Polygon(concealing_points_pbc)
        full_concealing_line = LineString(concealing_points_pbc)
        if not full_concealing_polygon.is_valid:
            concealing_polygon = Polygon()
        else:
            concealing_polygons = split(sub_visual_field, full_concealing_line)
            # need to change maybe to bigger then 0.8
            if len(concealing_polygons) == 1 or full_concealing_polygon.area < 0.08:  # .geom_type == 'Polygon':
                concealing_polygon = Polygon()
            else:
                for concealing_polygon_temp, index in zip(concealing_polygons, range(len(concealing_polygons))):
                    if not concealing_polygon_temp.is_valid:
                        concealing_polygons[index] = concealing_polygon_temp.buffer(0)
                concealing_polygon = concealing_polygons[
                    np.argmin([concealing_polygons[0].area, concealing_polygons[1].area])]
                break
    return concealing_polygon
