from classes import Arena, Type, Simulation
import random
import math
import csv
import ast
import agent_type_0
import agent_type_1
import agent_type_2
import agent_type_3
import agent_type_4
import agent_type_5


def create_arena():
    """input"""
    arena_size = [10, 10]

    """code"""
    arena = Arena(arena_size)

    return arena


def create_types():
    """input"""
    types_amount = 4

    """code"""
    types = []
    for type_number in range(types_amount):
        type_class = eval('agent_type_' +str(type_number) +'.create_type' + str(type_number) + '_class()')
        types.append(Type(type_number, "type" + str(type_number), type_class))

    return types


def create_types_with_amounts(agents_amount):
    types_amount = len(agents_amount)

    """code"""
    types = []
    for type_number in range(types_amount):
        type_class = eval('agent_type_' + str(type_number) + '.create_type' + str(type_number) + '_class()')
        type_class.amount = agents_amount[type_number]
        types.append(Type(type_number, "type" + str(type_number), type_class))

    return types


def create_simulation(types):
    """input"""
    length = 1000

    """code"""
    agents_amount_global = 0
    for type_number in range(len(types)):
        agents_amount_global += types[type_number].type_class.amount
    simulation = Simulation(length, agents_amount_global, length, [None], [None] * agents_amount_global,
                            [None] * agents_amount_global)

    return simulation


def create_agents(arena, types, simulation):
    """code"""
    agent_number_global = -1
    agents_polygons = [None] * simulation.agents_amount_global
    agents = [None] * simulation.agents_amount_global
    for type_number in range(len(types)):
        agent_numbers_global = agent_number_global + types[type_number].type_class.amount
        for agent_number_within_type in range(types[type_number].type_class.amount):
            agent_number_global = agent_number_global + 1
            agent_type = eval('agent_type_' + str(type_number) + '.create_' + types[
                type_number].name + '_agents(arena, agents_polygons, types[type_number].type_class, agent_number_global, agent_number_within_type)')
            agents[agent_number_global] = agent_type
            agents_polygons[agent_number_global] = agents[agent_number_global].polygon
    simulation.polygons = agents_polygons

    return agents, simulation


def rand_init_pos_and_angles(num_of_agents, arena):
    pos = []
    angles = []

    for i in range(num_of_agents):
        x = random.uniform(0, arena.size[0])
        y = random.uniform(0, arena.size[0])
        if [x, y] in pos:
            print("prob")
        pos.append([x, y])
        angles.append(random.uniform(math.pi / 180, 2 * math.pi))

    return pos, angles


def save_pos_and_angles_to_file(path, pos, angles):
    csvfile = open((path + '/pos_and_angles.csv'), 'w', newline='')
    obj = csv.writer(csvfile)
    obj.writerows([pos, angles])
    csvfile.close()


def read_init_pos_and_angles(path):  # from file
    csvfile = open((path + '/pos_and_angles.csv'), 'r', newline='')
    obj = csv.reader(csvfile)
    pos = next(obj)
    for i in range(len(pos)):
        pos[i] = ast.literal_eval(pos[i])
        pos[i] = list(map(float, pos[i]))
    angles = next(obj)
    angles = list(map(float, angles))
    csvfile.close()
    return pos, angles


def create_agents_with_fix_vec(arena, types, simulation, pos, angles):
    agent_number_global = -1
    agents_polygons = [None] * simulation.agents_amount_global
    agents = [None] * simulation.agents_amount_global
    for type_number in range(len(types)):
        agent_numbers_global = agent_number_global + types[type_number].type_class.amount
        for agent_number_within_type in range(types[type_number].type_class.amount):
            agent_number_global = agent_number_global + 1
            agent_type = eval('agent_type_' + str(type_number) + '.create_' + types[
                type_number].name + '_agents_fix(arena, agents_polygons, types[type_number].type_class, agent_number_global, agent_number_within_type, pos[agent_number_global], angles[agent_number_global])')
            agents[agent_number_global] = agent_type
            agents_polygons[agent_number_global] = agents[agent_number_global].polygon
    simulation.polygons = agents_polygons

    return agents, simulation
