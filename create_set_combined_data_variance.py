# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import sys
import sim_data

set_path = sys.argv[1]
# set_path = "Eden_Experiments/10_agents_same_type/set1"


sim_data.create_set_variance_combined_graph(set_path)
