import numpy as np
import visualization as vis
import matplotlib.pyplot as plt
import csv
import time
import math
import ast
from functions import calculate_corners, create_visual_field, correct4pbc, calculate_concealment, create_pbc_polygons
from classes import Simulation
import initiation
import re
import agent_type_0
import agent_type_1
import agent_type_2
import agent_type_3
import agent_type_4
import agent_type_5


def create_simulation(simulation, arena, types, agents, visualize, fileName):
    history = np.empty([simulation.steps_amount * 5, simulation.agents_amount_global])
    history[:] = np.nan
    for step in range(simulation.length):
        start = time.time()
        print('step: ', step)
        rand_vec_agents = list(range(simulation.agents_amount_global))  # asynchronous update
        sorted_rand_vec_agents = np.sort(rand_vec_agents)
        last_step_polygons = simulation.polygons
        last_step_agents = agents
        for agent_number_global in sorted_rand_vec_agents:
            agent = agents[agent_number_global]
            new_agent, visual_field = eval('agent_type_' + str (agent.type) +'.move_' + types[
                agent.type].name + '_agents(agent, agents, arena, last_step_polygons, types[agent.type].type_class)')
            agents[agent_number_global] = new_agent
            simulation.polygons[agent_number_global] = agents[agent_number_global].polygon
            simulation.visual_fields[agent_number_global] = visual_field

            history[step * 5 - 4][agent_number_global] = agent.position[0]
            history[step * 5 - 3][agent_number_global] = agent.position[1]
            history[step * 5 - 2][agent_number_global] = agent.angle
            history[step * 5 - 1][agent_number_global] = agent.is_walking
        end = time.time()
        history[step * 5 - 5][agent_number_global] = end - start

        if visualize:
            if step == 0:
                fig = plt.figure(figsize=(9, 8))
                axs = fig.add_subplot(111)
                axs.set_aspect('equal')
            axs = vis.visualization(axs, simulation, arena.size)
            fig.canvas.draw()
            plt.gca().clear()
    file = open(fileName, 'w')
    # file.write(fileName, 'w') as csvFile:
    writer = csv.writer(file)
    writer.writerows(history)
    file.close()

    return agents, simulation


def create_exp_simulation(simulation, arena, types, agents, visualize, fileName, pos, angles):
    history = np.empty([simulation.steps_amount * 5, simulation.agents_amount_global])
    history[:] = np.nan

    # csvfile = open('data.csv', 'w', newline='')
    # obj = csv.writer(csvfile)

    changes_per_step = []
    angles_per_step = []
    pos_per_step = []

    changes_per_step.append(len(agents))
    angles_per_step.append(angles)
    pos_per_step.append(pos)

    for step in range(simulation.length):
        angle_changes = 0
        all_angles = []
        all_pos = []
        all_pos = []
        Variance = 0

        start = time.time()
        print('step: ', step)

        last_step_polygons = simulation.polygons
        last_step_agents = agents

        for agent_number_global in range(simulation.agents_amount_global):
            agent = agents[agent_number_global]
            old_angle = agent.angle

            new_agent, visual_field = eval('agent_type_' + str (agent.type) +'.move_' + types[
                agent.type].name + '_agents(agent, agents, arena, last_step_polygons, types[agent.type].type_class)')
            agents[agent_number_global] = new_agent
            simulation.polygons[agent_number_global] = agents[agent_number_global].polygon
            simulation.visual_fields[agent_number_global] = visual_field
            new_angle = new_agent.angle

            if not old_angle == new_angle:
                angle_changes = angle_changes + 1

            all_angles.append(new_angle)
            all_pos.append(new_agent.position)

            history[step * 5 - 4][agent_number_global] = agent.position[0]
            history[step * 5 - 3][agent_number_global] = agent.position[1]
            history[step * 5 - 2][agent_number_global] = agent.angle
            history[step * 5 - 1][agent_number_global] = agent.is_walking
        end = time.time()
        history[step * 5 - 5][agent_number_global] = end - start

        if visualize:
            if step == 0:
                fig = plt.figure(figsize=(9, 8))
                axs = fig.add_subplot(111)
                axs.set_aspect('equal')
            axs = vis.visualization(axs, simulation, arena.size)
            fig.canvas.draw()
            plt.gca().clear()

        angles_per_step.append(all_angles)
        pos_per_step.append(all_pos)
        changes_per_step.append(angle_changes)

    return agents, simulation, changes_per_step, angles_per_step, pos_per_step


def read_data_from_file(path):
    csvfile = open((path + '/exp_data.csv'), 'r', newline='')
    obj = csv.reader(csvfile)
    changes_per_step = next(obj)
    changes_per_step = list(map(int, changes_per_step))
    angles_per_step = next(obj)
    for i in range(len(angles_per_step)):
        angles_per_step[i] = ast.literal_eval(angles_per_step[i])
        angles_per_step[i] = list(map(float, angles_per_step[i]))
    pos_per_step = next(obj)
    for step in range(len(pos_per_step)):
        pos_per_step[step] = pos_per_step[step].replace("[array(", "")
        pos_per_step[step] = pos_per_step[step].replace("array(", "")
        pos_per_step[step] = pos_per_step[step].replace(")]", "")
        pos_per_step[step] = pos_per_step[step].replace(")", "")
        pos_per_step[step] = pos_per_step[step].replace(" ", "")
        pos_per_step[step] = pos_per_step[step].replace("],", "]|")
        pos_per_step[step] = pos_per_step[step].split("|")
        for i in range(len(pos_per_step[step])):
            pos_per_step[step][i] = ast.literal_eval(pos_per_step[step][i])
        # pos_per_step[i] = list(map(float, pos_per_step[i]))
    csvfile.close()
    return changes_per_step, angles_per_step, pos_per_step


def restore_simulation(data_file_name):
    changes_per_step, angles_per_step, pos_per_step = read_data_from_file(data_file_name)

    # if change the agent_dimensions need to change this also
    agent_size = [0.1, 0.4]
    agent_center2edge = [0.5 * agent_size[0], 0.5 * agent_size[1]]
    agent_alpha = math.atan(agent_center2edge[0] / agent_center2edge[1])
    agent_dimensions = {"size": agent_size, "alpha": agent_alpha, "center2edge": agent_center2edge}
    visual_range = 1
    simulation_length = len(angles_per_step)
    agents_amount = len(angles_per_step[0])
    simulation = Simulation(simulation_length, agents_amount, simulation_length, [None], [None] * agents_amount,
                            [None] * agents_amount)
    arena = initiation.create_arena()

    for step in range(simulation_length):
        start = time.time()
        print('step: ', step)

        angles = angles_per_step[step]
        pos = pos_per_step[step]

        for agent_number_global in range(agents_amount):

            agent_new_position = pos[agent_number_global]
            # print("call from restore")
            agent_new_position_pbc = np.squeeze(correct4pbc([agent_new_position], arena.size)[-1])
            agent_corners = calculate_corners(agent_dimensions['alpha'], angles[agent_number_global],
                                              agent_new_position_pbc,
                                              agent_dimensions['center2edge'], arena.size)
            agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
            # visual_field_not_concealed = create_visual_field(agent_new_position, visual_range, arena)
            # visual_field = calculate_concealment(agents[agent_number_global], agents, visual_field_not_concealed,
            #                                      agents_polygons, type0, arena)
            visual_field = create_visual_field(agent_new_position, visual_range, arena)

            simulation.polygons[agent_number_global] = agent_polygon
            simulation.visual_fields[agent_number_global] = visual_field


        if step == 0:
            fig = plt.figure(figsize=(9, 8))
            axs = fig.add_subplot(111)
            axs.set_aspect('equal')
        axs = vis.visualization(axs, simulation, arena.size)
        fig.canvas.draw()
        plt.gca().clear()
