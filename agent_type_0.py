import random
import math
from functions import calculate_corners, create_rtree, check_collisions, create_visual_field, weighted_circmean, \
    circmean_r_length, correct4pbc, calculate_concealment, create_pbc_polygons
from classes import Agent
import numpy as np
from scipy.stats import circvar, circmean


def create_type0_class():
    """input"""

    agents_amount = 20
    agent_size = [0.1, 0.4]
    agent_speed = 0.1
    agent_prob2walk = 0.4
    agent_prob2stop = 0.2
    angle_visual_range = 1
    agent_memory_length = 5
    agent_alone_memory_length = 5  # if 0 always 0.4 (only now)
    agent_angle_inertia = 0.2
    agent_angle_surrounding = 0.3
    agent_angle_memory = 0.2
    prob2walk_alone = 0.8
    """code"""
    agent_center2edge = [0.5 * agent_size[0], 0.5 * agent_size[1]]
    agent_alpha = math.atan(agent_center2edge[0] / agent_center2edge[1])
    agent_dimensions = {"size": agent_size, "alpha": agent_alpha, "center2edge": agent_center2edge}
    type0 = Type0(agents_amount, agent_dimensions, agent_speed, agent_prob2walk, agent_prob2stop, angle_visual_range,
                  agent_memory_length, agent_alone_memory_length, agent_angle_inertia, agent_angle_surrounding,
                  agent_angle_memory, prob2walk_alone)
    return type0


class Type0:
    """code"""

    def __init__(self, agents_amount, agent_dimensions, agent_speed, agent_prob2walk, agent_prob2stop,
                 angle_visual_range, agent_memory_length, agent_alone_memory_length, agent_angle_inertia,
                 agent_angle_surrounding, agent_angle_memory, prob2walk_alone):
        self.amount = agents_amount
        self.dimensions = agent_dimensions
        self.speed = agent_speed
        self.prob2walk = agent_prob2walk
        self.prob2stop = agent_prob2stop
        self.visual_range = angle_visual_range
        self.memory_length = agent_memory_length
        self.alone_memory_length = agent_alone_memory_length
        self.angle_inertia = agent_angle_inertia
        self.angle_surrounding = agent_angle_surrounding
        self.angle_memory = agent_angle_memory
        self.angle_error = 1 - agent_angle_memory - agent_angle_surrounding - agent_angle_inertia
        self.prob2walk_alone = prob2walk_alone


def create_type0_agents(arena, agents_polygons, type0, agent_number_global, agent_number_within_type):  # only one agent
    """code"""
    intersects = True
    agent_is_walking = random.randint(0, 1)
    while intersects:
        agent_position = [random.uniform(0, arena.size[0]), random.uniform(0, arena.size[1])]
        agent_angle = random.uniform(math.pi / 180, 2 * math.pi)

        agent_corners = calculate_corners(type0.dimensions['alpha'], agent_angle,
                                          agent_position, type0.dimensions['center2edge'],
                                          arena.size)

        agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
        intersects = check_collisions(agents_polygons[:agent_number_global], agent_polygon, agent_number_global)
    agent_type0 = Agent(agent_number_global, 0, agent_number_within_type, agent_position,
                        agent_polygon, agent_angle, agent_is_walking, [None], [None] * (type0.memory_length + 1),
                        [None] * (type0.alone_memory_length + 1))
    return agent_type0


def create_type0_agents_fix(arena, agents_polygons, type0, agent_number_global, agent_number_within_type, pos, angle):  # only one agent
    """code"""
    intersects = True
    agent_is_walking = random.randint(0, 1)
    agent_position = pos
    agent_angle = angle

    agent_corners = calculate_corners(type0.dimensions['alpha'], agent_angle,
                                      agent_position, type0.dimensions['center2edge'],
                                      arena.size)

    agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
    agent_type0 = Agent(agent_number_global, 0, agent_number_within_type, agent_position,
                        agent_polygon, agent_angle, agent_is_walking, [None], [None] * (type0.memory_length + 1),
                        [None] * (type0.alone_memory_length + 1))
    return agent_type0


def create_type0_worldview(agent_number_global, agent_position, agents, arena, agents_polygons, type0):  # in every step
    agents_rtree = create_rtree(agents_polygons, agent_number_global)
    visual_field_not_concealed = create_visual_field(agent_position, type0.visual_range, arena)
    visual_field = calculate_concealment(agents[agent_number_global], agents, visual_field_not_concealed,
                                         agents_polygons, type0, arena)
    intersecting_agents = []
    i = -1
    for sub_visual_field in visual_field:
        i += 1
        if agents_rtree.query(sub_visual_field):
            for other_agent in agents:
                if other_agent.number_global == agent_number_global:
                    continue
                else:
                    for other_agent_sub_polygon in other_agent.polygon:
                        if sub_visual_field.intersects(other_agent_sub_polygon):
                            intersecting_agents.append(
                                {"number_global": other_agent.number_global, "is_walking": other_agent.is_walking,
                                 "angle": other_agent.angle})
    return intersecting_agents, visual_field  #


def move_type0_agents(agent, agents, arena, agents_polygons, type0):  # loop
    """code"""
    intersecting_agents, visual_field = create_type0_worldview(agent.number_global, agent.position, agents, arena,
                                                               agents_polygons,
                                                               type0)
    agent.memory.pop(0)  # history for angles of other agents
    agent.alone_memory.pop(0)  # history - if i saw something in the last steps
    agent.memory.append(intersecting_agents)
    if intersecting_agents:
        agent.alone_memory.append(1)  # add the value 1
    else:
        agent.alone_memory.append([])
    if agent.is_walking and (any(agent.alone_memory[:-1]) or type0.alone_memory_length == 0):
        if random.random() < type0.prob2walk:
            agent.is_walking = 1
        else:
            agent.is_walking = 0
        agent_new_angle = agent.angle
    elif agent.is_walking and not any(agent.alone_memory[:-1]):
        if random.random() < type0.prob2walk_alone:
            agent.is_walking = 1
        else:
            agent.is_walking = 0
        agent_new_angle = agent.angle
    elif not agent.is_walking:
        field_var = 0
        field_angles = []
        if intersecting_agents:
            for intersecting_agent in intersecting_agents:
                if intersecting_agent["is_walking"]:
                    field_angles.append((intersecting_agent["angle"]))
            # field_var = circmean_r_length(field_angles) # not fixed stop
        # if random.random() < (type0.prob2stop - field_var):  # not fixed stop
        if random.random() < (type0.prob2stop):  # fixed stop
            agent.is_walking = 0
        else:
            agent.is_walking = 1
        if agent.is_walking:
            field_mean_angle = circmean(field_angles)  # only last step
            memory_angles = []
            for memory in agent.memory[0:-1]:
                if memory:
                    for intersecting_agent in memory:
                        memory_angles.append(intersecting_agent["angle"])
            memory_mean_angle = circmean(memory_angles)  # according to -all- steps i remember
            weights = [type0.angle_inertia, type0.angle_memory, type0.angle_surrounding, type0.angle_error]
            error_angle = random.uniform(math.pi / 180, 2 * math.pi)
            angles = [agent.angle, field_mean_angle, memory_mean_angle, error_angle]
            agent_new_angle = weighted_circmean(weights, angles)
    if agent.is_walking:
        agent_new_position = [agent.position[0] + math.cos(agent_new_angle) * type0.speed,
                              agent.position[1] + math.sin(agent_new_angle) * type0.speed]
        agent_new_position_pbc = np.squeeze(correct4pbc([agent_new_position], arena.size)[-1])
        agent_corners = calculate_corners(type0.dimensions['alpha'], agent_new_angle,
                                          agent_new_position_pbc,
                                          type0.dimensions['center2edge'], arena.size)
        agent_polygon = create_pbc_polygons(agent_corners, arena.polygon)
        collisions = check_collisions(agents_polygons, agent_polygon, agent.number_global)
        if collisions:   # ????
            agent.is_walking = 0
        else:
            agent.angle = agent_new_angle
            agent.position = agent_new_position_pbc
            agent.polygon = agent_polygon
    return agent, visual_field
