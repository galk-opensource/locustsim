# Eden's branch
import initiation
import simulate
import os
import math
import csv
import matplotlib.pyplot as plt
from pandas import DataFrame
import ast
from scipy.stats import circvar, circmean
import sim_data


# folders structure:
#
#   Eden_Experiments
#       type1
#           set1 (of angles and positions)
#               model_x
#                   run1






path = "Eden_Experiments"
os.mkdir(path)

# exp_types = [10, 20, 30]

exp_types = [20, 30]

curr_path = path

for exp_type in exp_types:

    num_of_agents = int(exp_type)
    exp_type_name = str(exp_type) + "_agents_same_type"
    curr_path = path + "/" + exp_type_name
    os.mkdir(curr_path)

    num_of_sets = 10
    for i in range(num_of_sets):
        curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1))
        os.mkdir(curr_path)
        arena = initiation.create_arena()
        pos, angles = initiation.rand_init_pos_and_angles(num_of_agents, arena)
        initiation.save_pos_and_angles_to_file(curr_path, pos, angles)

        # type 0 - Daniel's model
        # type 2 - handles both crowded and lonely
        # type 3 - align all the time
        # type 4 - handles only crowded
        # type 5 - handles only lonely

        agents_sets = [["daniel's model", [num_of_agents, 0, 0, 0, 0, 0]],
                       ["handles crowded & lonely", [0, 0, num_of_agents, 0, 0, 0]],
                       ["always align", [0, 0, 0, num_of_agents, 0, 0]],
                       ["handles only crowded", [0, 0, 0, 0, num_of_agents, 0]],
                       ["handles only lonely", [0, 0, 0, 0, 0, num_of_agents]]]

        for agents_set in agents_sets:
            model_name = agents_set[0]
            agents_amount = agents_set[1]

            curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1)) + "/" + model_name
            os.mkdir(curr_path)

            num_of_runs = 1
            if model_name == "daniel's model":
                num_of_runs = 5
            for j in range(num_of_runs):

                curr_path = path + "/" + exp_type_name + "/" + ("set" + str(i + 1)) + "/" + model_name + "/" + ("run" + str(j))
                os.mkdir(curr_path)
                # pos, angles = initiation.read_init_pos_and_angles(path)  # from file

                types = initiation.create_types_with_amounts(agents_amount)
                simulation = initiation.create_simulation(types)
                agents, simulation = initiation.create_agents_with_fix_vec(arena, types, simulation, pos, angles)
                fileName = 'fileName.csv'
                agents, simulation, changes_per_step, angles_per_step, pos_per_step =\
                    simulate.create_exp_simulation(simulation, arena, types, agents, 0, fileName, pos, angles)
                sim_data.save_data_to_file(curr_path, changes_per_step, angles_per_step, pos_per_step)
                sim_data.create_graphs(curr_path, changes_per_step, angles_per_step)





